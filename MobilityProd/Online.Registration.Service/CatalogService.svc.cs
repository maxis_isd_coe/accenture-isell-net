﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;

using SNT.Utility;

using Online.Registration.Service.Models;
using Online.Registration.DAL.Admin;
using Online.Registration.DAL.Models;
using System;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CatalogService" in code, svc and config file together.
    public class CatalogService : ICatalogService
    {
        static string svcName = "CatalogService";
        private static readonly ILog Logger = LogManager.GetLogger(svcName); 
        public CatalogService()
        {
            //if (log4net.LogManager.GetCurrentLoggers().Count() == 0)
            //{
            //    log4net.Config.XmlConfigurator.Configure();
            //}
            Logger.InfoFormat("{0} initialized....", svcName);
        }

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.StackTrace = ex.StackTrace;
        }

        #endregion

        #region Program

        public ProgramCreateResp ProgramCreate(ProgramCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramCreate({1})", svcName, oReq.Program.Name);

            ProgramCreateResp resp = new ProgramCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.ProgramCreate(oReq.Program);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                ////TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ProgramCreate({1}): {2}|{3}", svcName, oReq.Program.Name, resp.ID, resp.Code);
            }

            return resp;
        }
        public ProgramUpdateResp ProgramUpdate(ProgramUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramUpdate({1})", svcName, oReq.Program.ID);
            ProgramUpdateResp resp = new ProgramUpdateResp();

            try
            {
                int count = OnlineRegAdmin.ProgramUpdate(oReq.Program);
                Logger.DebugFormat("{0}.OnlineRegAdmin.ProgramUpdate({1}):{2}", svcName, oReq.Program.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ProgramUpdate({1}): {2}", svcName, oReq.Program.ID, resp.Code);
            }

            return resp;
        }
        public ProgramFindResp ProgramFind(ProgramFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramFind({1})", svcName, (oReq != null) ? oReq.ProgramFind.Program.Name : "NULL");
            var resp = new ProgramFindResp();

            try { resp.IDs = OnlineRegAdmin.ProgramFind(oReq.ProgramFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ProgramFind({1}): {2}", svcName, (oReq != null) ? oReq.ProgramFind.Program.Name : "NULL", resp.Code);
            }

            return resp;
        }
        public ProgramGetResp ProgramGet(ProgramGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ProgramGet({1})", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0);
            ProgramGetResp resp = new ProgramGetResp();

            try
            {
                resp.Programs = OnlineRegAdmin.ProgramGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ProgramGet({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        public ProgramGetResp ProgramsList()
        {
            Logger.InfoFormat("Entering {0}.ProgramsList({1})", svcName, "");
            ProgramGetResp resp = new ProgramGetResp();
            try
            {
                resp.Programs = OnlineRegAdmin.ProgramsList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ProgramsList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        #region Brand
        public BrandCreateResp BrandCreate(BrandCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandCreate({1})", svcName, oReq.Brand.Name);
            BrandCreateResp oRp = new BrandCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.BrandCreate(oReq.Brand);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandCreate({1}): {2}|{3}", svcName, oReq.Brand.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public BrandUpdateResp BrandUpdate(BrandUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandUpdate({1})", svcName, oReq.Brand.Name);
            BrandUpdateResp oRp = new BrandUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.BrandUpdate(oReq.Brand);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Brand update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandUpdate({1}): {2}", svcName, oReq.Brand.ID, oRp.Code);
            }
            return oRp;
        }
        public BrandGetResp BrandGet(BrandGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            BrandGetResp oRp = new BrandGetResp();
            try
            {
                oRp.Brands = OnlineRegAdmin.BrandGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public BrandGetRespSmart BrandGetSmart(BrandGetReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            BrandGetRespSmart oRp = new BrandGetRespSmart();
            try
            {
                oRp.Brands = OnlineRegAdmin.BrandGetSmart(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public BrandGetResp BrandsList()
        {
            Logger.InfoFormat("Entering {0}.BrandsList({1})", svcName, "");
            BrandGetResp oRp = new BrandGetResp();
            try
            {
                oRp.Brands = OnlineRegAdmin.BrandsList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandsList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public BrandFindResp BrandFind(BrandFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, oReq.BrandFind);
            BrandFindResp oRp = new BrandFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.BrandFind(oReq.BrandFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, oReq.BrandFind.Brand.Name, oRp.Code);
            }
            return oRp;
        }
        public BrandFindRespSmart BrandFindSmart(BrandFindReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, oReq.BrandFind);
            BrandFindRespSmart oRp = new BrandFindRespSmart();
            try
            {
                oRp.IDs = OnlineRegAdmin.BrandFindSmart(oReq.BrandFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, oReq.BrandFind.Brand.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Category

        public CategoryCreateResp CategoryCreate(CategoryCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryCreate({1})", svcName, oReq.Category.Name);
            CategoryCreateResp oRp = new CategoryCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.CategoryCreate(oReq.Category);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CategoryCreate({1}): {2}|{3}", svcName, oReq.Category.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public CategoryUpdateResp CategoryUpdate(CategoryUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryUpdate({1})", svcName, oReq.Category.Name);
            CategoryUpdateResp oRp = new CategoryUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.CategoryUpdate(oReq.Category);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Category update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CategoryUpdate({1}): {2}", svcName, oReq.Category.ID, oRp.Code);
            }
            return oRp;
        }
        public CategoryGetResp CategoryGet(CategoryGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryGet({1})", svcName, oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            CategoryGetResp oRp = new CategoryGetResp();
            try
            {
                oRp.Categorys = OnlineRegAdmin.CategoryGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CategoryGet({1}): {2}", svcName, (oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public CategoryGetResp CategoriesList()
        {
            Logger.InfoFormat("Entering {0}.CategoryGet({1})", svcName, "");
            CategoryGetResp oRp = new CategoryGetResp();
            try
            {
                oRp.Categorys = OnlineRegAdmin.CategoriesList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CategoryGet({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public CategoryFindResp CategoryFind(CategoryFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.CategoryFind({1})", svcName, oReq.CategoryFind);
            CategoryFindResp oRp = new CategoryFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CategoryFind(oReq.CategoryFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.CategoryFind({1}): {2}", svcName, oReq.CategoryFind.Category.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Bundle

        public BundleCreateResp BundleCreate(BundleCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BundleCreate({1})", svcName, oReq.Bundle.Name);

            var resp = new BundleCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.BundleCreate(oReq.Bundle);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BundleCreate({1}): {2}|{3}", svcName, oReq.Bundle.Name, resp.ID, resp.Code);
            }

            return resp;
        }

        public BundleUpdateResp BundleUpdate(BundleUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BundleUpdate({1})", svcName, oReq.Bundle.ID);

            var resp = new BundleUpdateResp();

            try
            {
                int count = OnlineRegAdmin.BundleUpdate(oReq.Bundle);
                Logger.DebugFormat("{0}.OnlineRegAdmin.BundleUpdate({1}):{2}", svcName, oReq.Bundle.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BundleUpdate({1}): {2}", svcName, oReq.Bundle.ID, resp.Code);
            }

            return resp;
        }

        public BundleFindResp BundleFind(BundleFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BundleFind({1})", svcName, oReq.BundleFind.Bundle.Name);
            BundleFindResp resp = new BundleFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.BundleFind(oReq.BundleFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BundleFind({1}): {2}", svcName, oReq.BundleFind.Bundle.Name, resp.Code);
            }

            return resp;
        }

        public BundleGetResp BundleGet(BundleGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BundleGet({1})", svcName, oReq.IDs.Count());
            BundleGetResp resp = new BundleGetResp();

            try
            {
                resp.Bundles = OnlineRegAdmin.BundleGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BundleGet({1}): {2}", svcName, oReq.IDs.Count(), resp.Code);
            }

            return resp;
        }

        public BundleGetResp BundlesList()
        {
            Logger.InfoFormat("Entering {0}.BundlesList({1})", svcName, "");
            BundleGetResp resp = new BundleGetResp();
            try
            {
                resp.Bundles = OnlineRegAdmin.BundlesList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BundlesList({1}): {2}", svcName, "", resp.Code);
            }
            return resp;
        }

        #endregion

        #region Package
        public Int32 GettllnkContractDataPlan(int selectedid)
        {
            //** Updated by Chetan on 20/12/2013
            Logger.InfoFormat("Entering {0}.GettllnkContractDataPlan({1})", selectedid, svcName);
            Int32 value = 0;
            try
            {
                value = OnlineRegAdmin.GettllnkContractDataPlan(selectedid);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GettllnkContractDataPlan({1})", svcName, selectedid);
            }
            return value;
        }

        public List<int> GettllnkContractDataPlanList(List<int> selectedids)
        {
            Logger.InfoFormat("Entering {0}.GettllnkContractDataPlan({1})",svcName, string.Join(",", selectedids));
            List<int> value = new List<int>();
            try
            {
                value = OnlineRegAdmin.GettllnkContractDataPlanList(selectedids);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GettllnkContractDataPlan({1})", svcName, string.Join(",", selectedids));
            }
            return value;
        }

        public List<int> GetDependentComponentList()
        {
            Logger.InfoFormat("Entering GetAllDependentComponents()");
            List<Int32> value = new List<int>();
            try
            {
                value = OnlineRegAdmin.GetAllDependentComponents();
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                Logger.Error("GetAllDependentComponents", ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting GetAllDependentComponents()");
            }
            return value;
        }

        public List<Int32> GetDependentComponents(int componentId)
        {
            Logger.InfoFormat("Entering {0}.GetDependentComponents({1})",svcName, componentId);
            List<Int32> value = new List<int>();
            try
            {
                value = OnlineRegAdmin.GetDependentComponents(componentId);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetDependentComponents({1})", svcName, componentId);
            }
            return value;
        }
        public List<dependentContract> GetContractsOnOfferId(string offerIds)
        {
            Logger.InfoFormat("Entering {0}.GetContractsOnOfferId({1})", svcName.ToString2(), offerIds);
            List<dependentContract> value = new List<dependentContract>();
            try
            {
                value = OnlineRegAdmin.GetContractsOnOfferId(offerIds);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.GetContractsOnOfferId({1})", svcName.ToString2(), offerIds);
            }
            return value;
        }
        public DataPlanIdsResp getFilterplanvalues(int plantypeid)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGet({1})", svcName, plantypeid);
            DataPlanIdsResp value = new DataPlanIdsResp();
            try
            {
                value.values = OnlineRegAdmin.Getdataplanpackageids(plantypeid);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGet({1})", svcName, plantypeid);
            }
            return value;
        }
        public BundlepackageResp GetMandatoryVas(int selectedid)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGet({1})", svcName, selectedid);
            BundlepackageResp value = new BundlepackageResp();
            try
            {
                value.values = OnlineRegAdmin.GetMandatoryVas(selectedid);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGet({1})", svcName, selectedid);
            }
            return value;
        }
        public PackageCreateResp PackageCreate(PackageCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageCreate({1})", svcName, oReq.Package.Name);
            PackageCreateResp oRp = new PackageCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.PackageCreate(oReq.Package);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

                //throw;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageCreate({1})", svcName, oReq.Package.Name);
            }
            return oRp;
        }
        public PackageUpdateResp PackageUpdate(PackageUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageUpdate({1})", svcName, oReq.Package.Name);
            PackageUpdateResp oRp = new PackageUpdateResp();
            try
            {
                OnlineRegAdmin.PackageUpdate(oReq.Package);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;                
                //oRp.StackTrace = ex.StackTrace;
                //throw;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageUpdate({1})", svcName, oReq.Package.Name);
            }
            return oRp;
        }
        public PackageGetResp PackageGet(PackageGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageGet({1})", svcName, oReq.IDs.Count().ToString2());
            PackageGetResp oRp = new PackageGetResp();
            try
            {
                oRp.Packages = OnlineRegAdmin.PackageGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageGet({1})", svcName, oReq.IDs.Count().ToString2());
            }
            return oRp;
        }

        public PackageGetResp PackagesList()
        {
            Logger.InfoFormat("Entering {0}.PackagesList({1})", svcName, "");
            PackageGetResp oRp = new PackageGetResp();
            try
            {
                oRp.Packages = OnlineRegAdmin.PackagesList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackagesList({1})", svcName, "");
            }
            return oRp;
        }

        public PackageFindResp PackageFind(PackageFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageFind({1})", svcName, oReq.PackageFind);
            PackageFindResp oRp = new PackageFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PackageFind(oReq.PackageFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageFind({1}): {2}", svcName, oReq.PackageFind.Package.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Package Type
        public PackageTypeCreateResp PackageTypeCreate(PackageTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageTypeCreate({1})", svcName, oReq.PackageType.Name);
            PackageTypeCreateResp oRp = new PackageTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.PackageTypeCreate(oReq.PackageType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;
                //throw;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageTypeCreate({1})", svcName, oReq.PackageType.Name);
            }
            return oRp;
        }
        public PackageTypeUpdateResp PackageTypeUpdate(PackageTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageTypeUpdate({1})", svcName, oReq.PackageType.Name);
            PackageTypeUpdateResp oRp = new PackageTypeUpdateResp();
            try
            {
                OnlineRegAdmin.PackageTypeUpdate(oReq.PackageType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;
                //throw;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageTypeUpdate({1})", svcName, oReq.PackageType.Name);
            }
            return oRp;
        }
        public PackageTypeGetResp PackageTypeGet(PackageTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageTypeGet({1})", svcName, oReq.IDs.Count().ToString2());
            PackageTypeGetResp oRp = new PackageTypeGetResp();
            try
            {
                oRp.PackageTypes = OnlineRegAdmin.PackageTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageTypeGet({1})", svcName, oReq.IDs.Count().ToString2());
            }
            return oRp;
        }

        public PackageTypeGetResp PackageTypesList()
        {
            Logger.InfoFormat("Entering {0}.PackageTypeGet({1})", svcName, "");
            PackageTypeGetResp oRp = new PackageTypeGetResp();
            try
            {
                oRp.PackageTypes = OnlineRegAdmin.PackageTypesList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
                throw ex;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageTypeGet({1})", svcName, "");
            }
            return oRp;
        }
        public PackageTypeFindResp PackageTypeFind(PackageTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PackageTypeFind({1})", svcName, oReq.PackageTypeFind);
            PackageTypeFindResp oRp = new PackageTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PackageTypeFind(oReq.PackageTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PackageTypeFind({1}): {2}", svcName, oReq.PackageTypeFind.PackageType.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Colour
        public ColourCreateResp ColourCreate(ColourCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourCreate({1})", svcName, oReq.Colour.Name);
            ColourCreateResp oRp = new ColourCreateResp();
            try
            {
                oRp.ColourID = OnlineRegAdmin.ColourCreate(oReq.Colour);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ColourID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ColourCreate({1}): {2}|{3}", svcName, oReq.Colour.Name, oRp.ColourID, oRp.Code);
            }
            return oRp;
        }
        public ColourUpdateResp ColourUpdate(ColourUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourUpdate({1})", svcName, oReq.Colour.Name);
            ColourUpdateResp oRp = new ColourUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ColourUpdate(oReq.Colour);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Colour update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ColourUpdate({1}): {2}", svcName, oReq.Colour.ID, oRp.Code);
            }
            return oRp;
        }
        public ColourGetResp ColourGet(ColourGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourGet({1})", svcName, (oReq.IDs != null || oReq.IDs.Count() != 0) ? 0 : oReq.IDs[0]);
            ColourGetResp oRp = new ColourGetResp();
            try
            {
                oRp.Colours = OnlineRegAdmin.ColourGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ColourGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ColourGetResp ColoursList()
        {
            Logger.InfoFormat("Entering {0}.ColoursList({1})", svcName, "");
            ColourGetResp oRp = new ColourGetResp();
            try
            {
                oRp.Colours = OnlineRegAdmin.ColoursList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ColourGet({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }
        public ColourFindResp ColourFind(ColourFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ColourFind({1})", svcName, oReq.ColourFind);
            ColourFindResp oRp = new ColourFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ColourFind(oReq.ColourFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ColourFind({1}): {2}", svcName, oReq.ColourFind.Colour.Name, oRp.Code);
            }
            return oRp;
        }

        #endregion

        #region Component
        public ComponentCreateResp ComponentCreate(ComponentCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentCreate({1})", svcName, oReq.Component.Name);
            ComponentCreateResp oRp = new ComponentCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ComponentCreate(oReq.Component);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentCreate({1}): {2}|{3}", svcName, oReq.Component.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ComponentUpdateResp ComponentUpdate(ComponentUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentUpdate({1})", svcName, oReq.Component.Name);
            ComponentUpdateResp oRp = new ComponentUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ComponentUpdate(oReq.Component);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Component update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentUpdate({1}): {2}", svcName, oReq.Component.Name, oRp.Code);
            }
            return oRp;
        }
        public ComponentGetResp ComponentGet(ComponentGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentGet({1})", svcName, oReq.IDs.Count());
            ComponentGetResp oRp = new ComponentGetResp();
            try
            {
                oRp.Components = OnlineRegAdmin.ComponentGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentGet({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ComponentFindResp ComponentFind(ComponentFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentFind({1})", svcName, oReq.ComponentFind);
            ComponentFindResp oRp = new ComponentFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ComponentFind(oReq.ComponentFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentFind({1}): {2}", svcName, oReq.ComponentFind.Component.Name, oRp.Code);
            }
            return oRp;
        }
        public ContractComponentGetResp ContractComponentGet()
        {
            Logger.InfoFormat("Entering {0}.ContractComponentGet({1})");
            ContractComponentGetResp oRp = new ContractComponentGetResp();
            try
            {
                oRp.Components = OnlineRegAdmin.ContractComponentGet();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ContractComponentGet({1}): {2}", svcName);
            }
            return oRp;
        }
        #endregion

        #region Component Type
        public ComponentTypeCreateResp ComponentTypeCreate(ComponentTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentTypeCreate({1})", svcName, oReq.ComponentType.Name);
            ComponentTypeCreateResp oRp = new ComponentTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ComponentTypeCreate(oReq.ComponentType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentTypeCreate({1}): {2}|{3}", svcName, oReq.ComponentType.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ComponentTypeUpdateResp ComponentTypeUpdate(ComponentTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentTypeUpdate({1})", svcName, oReq.ComponentType.Name);
            ComponentTypeUpdateResp oRp = new ComponentTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ComponentTypeUpdate(oReq.ComponentType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Component Type update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentTypeUpdate({1}): {2}", svcName, oReq.ComponentType.ID, oRp.Code);
            }
            return oRp;
        }
        public ComponentTypeGetResp ComponentTypeGet(ComponentTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentTypeGet({1})", svcName, oReq.IDs.Count());
            ComponentTypeGetResp oRp = new ComponentTypeGetResp();
            try
            {
                oRp.ComponentTypes = OnlineRegAdmin.ComponentTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentTypeGet({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ComponentTypeGetResp ComponentTypeList()
        {
            Logger.InfoFormat("Entering {0}.ComponentTypeList({1})", svcName, "");
            ComponentTypeGetResp oRp = new ComponentTypeGetResp();
            try
            {
                oRp.ComponentTypes = OnlineRegAdmin.ComponentTypeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentTypeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }
        public ComponentTypeFindResp ComponentTypeFind(ComponentTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ComponentTypeFind({1})", svcName, oReq.ComponentTypeFind);
            ComponentTypeFindResp oRp = new ComponentTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ComponentTypeFind(oReq.ComponentTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentTypeFind({1}): {2}", svcName, oReq.ComponentTypeFind.ComponentType.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Component Relation

		public List<ComponentRelation> GetAllComponentRelationByRule(string rules)
		{
			Logger.InfoFormat("Entering {0}.GetAllComponentRelationByRule(({1})", svcName, "ALL");
			var resp = new List<ComponentRelation>();
			try
			{
				resp = OnlineRegAdmin.GetAllComponentRelationByRule(rules);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("StackTrace: {1}", ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.GetAllComponentRelationByRule({1})", svcName, "ALL");
			}
			return resp;
		}

        public List<ComponentRelation> ComponentRelationGet(int planBundle, string rules)
        {
            Logger.InfoFormat("Entering {0}.ComponentRelationGet({1})", svcName, planBundle);
            var resp = new List<ComponentRelation>();
            try
            {
                resp = OnlineRegAdmin.ComponentRelationGet(planBundle, rules);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("StackTrace: {1}", ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ComponentRelationGet({1})", svcName, planBundle);
            }
            return resp;
        }

		public List<ComponentRelation> ComponentRelationGetByComponent(int componentPgm, string rules)
		{
			Logger.InfoFormat("Entering {0}.ComponentRelationGetByComponent({1})", svcName, componentPgm);
			var resp = new List<ComponentRelation>();
			try
			{
				resp = OnlineRegAdmin.ComponentRelationGetByComponent(componentPgm, rules);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("StackTrace - ComponentRelationGetByComponent: {1}", ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.ComponentRelationGetByComponent({1})", svcName, componentPgm);
			}
			return resp;
		}
        #endregion

		#region Plan Device VAS Validation
		public PlanDeviceValidationRulesGetResp PlanDeviceValidationRulesGet(PlanDeviceValidationRulesGetReq oReq)
		{
			Logger.InfoFormat("Entering {0}.planDeviceValidationRulesGet({1})", oReq.PlanDeviceValidationRules.existingPlanId, oReq.PlanDeviceValidationRules.existingDeviceArticleId, oReq.PlanDeviceValidationRules.existingVASId);
			var resp = new PlanDeviceValidationRulesGetResp();
			try
			{
				resp.PlanDeviceValidationRulesGet = OnlineRegAdmin.PlanDeviceValidationRulesGet(oReq.PlanDeviceValidationRules);
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("StackTrace: {1}", ex.StackTrace), ex);
			}
			finally
			{
				Logger.InfoFormat("Exiting {0}.planDeviceValidationRulesGet({1})", resp);
			}
			return resp;
		}
		#endregion

        #region Model
        public ModelCreateResp ModelCreate(ModelCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelCreate({1})", svcName, oReq.Model.Name);
            ModelCreateResp oRp = new ModelCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ModelCreate(oReq.Model);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelCreate({1}): {2}|{3}", svcName, oReq.Model.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelUpdateResp ModelUpdate(ModelUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelUpdate({1})", svcName, oReq.Model.Name);
            ModelUpdateResp oRp = new ModelUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ModelUpdate(oReq.Model);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Model update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelUpdate({1}): {2}", svcName, oReq.Model.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelGetResp ModelGet(ModelGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, oReq == null || oReq.IDs == null || oReq.IDs.Count() == 0 ? 0 : oReq.IDs[0]);
            ModelGetResp oRp = new ModelGetResp();
            try
            {
                oRp.Models = OnlineRegAdmin.ModelGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGet({1}): {2}", svcName, oReq == null || oReq.IDs == null || oReq.IDs.Count() == 0 ? 0 : oReq.IDs[0], oRp.Code);
            }
            return oRp;
        }
        public ModelGetRespSmart ModelGetSmart(ModelGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, oReq == null || oReq.IDs == null || oReq.IDs.Count() == 0 ? 0 : oReq.IDs[0]);
            ModelGetRespSmart oRp = new ModelGetRespSmart();
            try
            {
                oRp.Models = OnlineRegAdmin.ModelGetSmart(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGet({1}): {2}", svcName, oReq == null || oReq.IDs == null || oReq.IDs.Count() == 0 ? 0 : oReq.IDs[0], oRp.Code);
            }
            return oRp;
        }

        public ModelGetResp ModelsList()
        {
            Logger.InfoFormat("Entering {0}.ModelsList({1})", svcName, "");
            ModelGetResp oRp = new ModelGetResp();
            try
            {
                oRp.Models = OnlineRegAdmin.ModelsList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelsList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public ModelFindResp ModelFind(ModelFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelFind({1})", svcName, oReq.ModelFind);
            ModelFindResp oRp = new ModelFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelFind(oReq.ModelFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                if (!ReferenceEquals(oReq.ModelFind.Model, null))
                {
                    Logger.InfoFormat("Exiting {0}.ModelFind({1}): {2}", svcName, oReq.ModelFind.Model.Name, oRp.Code);
                }
                else
                {
                    Logger.InfoFormat("Exiting {0}.ModelFind({1}): {2}", svcName, "HERO DEVICE", oRp.Code);
                }
            }
            return oRp;
        }
        public ModelFindResp ModelFindSmart(ModelFindReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelFind({1})", svcName, oReq.ModelFind);
            ModelFindResp oRp = new ModelFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelFindSmart(oReq.ModelFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelFind({1}): {2}", svcName, oReq.ModelFind.Model.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region ModelImage
        public ModelImageCreateResp ModelImageCreate(ModelImageCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageCreate({1})", svcName, oReq.ModelImage.ImagePath);
            ModelImageCreateResp oRp = new ModelImageCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ModelImageCreate(oReq.ModelImage);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelImageCreate({1}): {2}|{3}", svcName, oReq.ModelImage.ImagePath, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelImageUpdateResp ModelImageUpdate(ModelImageUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageUpdate({1})", svcName, oReq.ModelImage.ImagePath);
            ModelImageUpdateResp oRp = new ModelImageUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ModelImageUpdate(oReq.ModelImage);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "ModelImage update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelImageUpdate({1}): {2}", svcName, oReq.ModelImage.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelImageGetResp ModelImageGet(ModelImageGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            ModelImageGetResp oRp = new ModelImageGetResp();
            try
            {
                oRp.ModelImages = OnlineRegAdmin.ModelImageGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelImageGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ModelImageFindResp ModelImageFind(ModelImageFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelImageFind({1})", svcName, oReq.ModelImageFind);
            ModelImageFindResp oRp = new ModelImageFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelImageFind(oReq.ModelImageFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelImageFind({1}): {2}", svcName, oReq.ModelImageFind.ModelImage.ImagePath, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Model Group
        public ModelGroupCreateResp ModelGroupCreate(ModelGroupCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupCreate({1})", svcName, oReq.ModelGroup.Name);
            ModelGroupCreateResp oRp = new ModelGroupCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ModelGroupCreate(oReq.ModelGroup);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupCreate({1}): {2}|{3}", svcName, oReq.ModelGroup.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupUpdateResp ModelGroupUpdate(ModelGroupUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupUpdate({1})", svcName, oReq.ModelGroup.Name);
            ModelGroupUpdateResp oRp = new ModelGroupUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ModelGroupUpdate(oReq.ModelGroup);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Model Group Package update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupUpdate({1}): {2}", svcName, oReq.ModelGroup.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupGetResp ModelGroupGet(ModelGroupGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupGet({1})", svcName, oReq.IDs.Count());
            ModelGroupGetResp oRp = new ModelGroupGetResp();
            try
            {
                oRp.ModelGroups = OnlineRegAdmin.ModelGroupGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupGet({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ModelGroupGetResp ModelGroupList()
        {
            Logger.InfoFormat("Entering {0}.ModelGroupList({1})", svcName, "");
            ModelGroupGetResp oRp = new ModelGroupGetResp();
            try
            {
                oRp.ModelGroups = OnlineRegAdmin.ModelGroupList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public ModelGroupGetResp ModelGroupGetByPBPCIDs(ModelGroupGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupGetByPkgIDs({1})", svcName, oReq.IDs.Count() > 0 ? oReq.IDs[0] : 0);
            ModelGroupGetResp oRp = new ModelGroupGetResp();
            try
            {
                oRp.ModelGroups = OnlineRegAdmin.ModelGroupGetByPBPCIDs(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupGetByPkgIDs({1}): {2}", svcName, (oReq.IDs.Count() > 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupFindResp ModelGroupFind(ModelGroupFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupFind({1})", svcName, oReq.ModelGroupFind);
            ModelGroupFindResp oRp = new ModelGroupFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelGroupFind(oReq.ModelGroupFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupFind({1}): {2}", svcName, oReq.ModelGroupFind.ModelGroup.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Program Bundle Package Component
        public PgmBdlPckComponentCreateResp PgmBdlPckComponentCreate(PgmBdlPckComponentCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentCreate({1})", svcName, oReq.PgmBdlPckComponent.ID);
            PgmBdlPckComponentCreateResp oRp = new PgmBdlPckComponentCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ProgramBundlePackageComponentCreate(oReq.PgmBdlPckComponent);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentCreate({1}): {2}|{3}", svcName, oReq.PgmBdlPckComponent.ID, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public PgmBdlPckComponentUpdateResp PgmBdlPckComponentUpdate(PgmBdlPckComponentUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentUpdate({1})", svcName, oReq.PgmBdlPckComponent.ID);
            PgmBdlPckComponentUpdateResp oRp = new PgmBdlPckComponentUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ProgramBundlePackageComponentUpdate(oReq.PgmBdlPckComponent);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Program Bundle Package Component update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentUpdate({1}): {2}", svcName, oReq.PgmBdlPckComponent.ID, oRp.Code);
            }
            return oRp;
        }
        public PgmBdlPckComponentGetResp PgmBdlPckComponentGet(PgmBdlPckComponentGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGet({1})", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0);
            PgmBdlPckComponentGetResp oRp = new PgmBdlPckComponentGetResp();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.ProgramBundlePackageComponentGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGet({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public PgmBdlPckComponentGetResp PgmBdlPckComponentGet1(PgmBdlPckComponentFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGet1", svcName);
            PgmBdlPckComponentGetResp oRp = new PgmBdlPckComponentGetResp();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.ProgramBundlePackageComponentGet1(oReq.PgmBdlPckComponentFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGet", svcName);
            }
            return oRp;
        }
        public PgmBdlPckComponentFindResp PgmBdlPckComponentFind(PgmBdlPckComponentFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentFind({1})", svcName, oReq.PgmBdlPckComponentFind);
            PgmBdlPckComponentFindResp oRp = new PgmBdlPckComponentFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ProgramBundlePackageComponentFind(oReq.PgmBdlPckComponentFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentFind({1}): {2}", svcName, oReq.PgmBdlPckComponentFind.PgmBdlPckComponent.ID, oRp.Code);
            }
            return oRp;
        }
        public PkgCompContractGetResp PkgCompContractGet()
        {
            Logger.InfoFormat("Entering {0}.PkgCompContractGet({1})", svcName);
            PkgCompContractGetResp oRp = new PkgCompContractGetResp();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.PkgCompContractGet();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PkgCompContractGet({1}): {2}", svcName, oRp.Code);
            }
            return oRp;
        }

        public BdlPkgGetResp BdlPkgGet()
        {
            Logger.InfoFormat("Entering {0}.BdlPkgGet({1})", svcName);
            BdlPkgGetResp oRp = new BdlPkgGetResp();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.BdlPkgGet();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BdlPkgGet({1}): {2}", svcName, oRp.Code);
            }
            return oRp;
        }

        public MISMMandatoryComponentGetResp MISMMandatoryComponentGet()
        {
            Logger.InfoFormat("Entering {0}.MISMMandatoryComponentGet()", svcName);
            var resp = new MISMMandatoryComponentGetResp();

            try
            {

                resp.MISMComps = OnlineRegAdmin.MISMMandatoryComponentGet();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.MISMMandatoryComponentGet(): {1}", svcName, resp.Code);
            }

            return resp;
        }
        #endregion

        #region Model Group Model
        public ModelGroupModelCreateResp ModelGroupModelCreate(ModelGroupModelCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelCreate({1})", svcName, oReq.ModelGroupModel.Name);
            ModelGroupModelCreateResp oRp = new ModelGroupModelCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ModelGroupModelCreate(oReq.ModelGroupModel);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelCreate({1}): {2}|{3}", svcName, oReq.ModelGroupModel.Name, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupModelUpdateResp ModelGroupModelUpdate(ModelGroupModelUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelUpdate({1})", svcName, oReq.ModelGroupModel.Name);
            ModelGroupModelUpdateResp oRp = new ModelGroupModelUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ModelGroupModelUpdate(oReq.ModelGroupModel);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Model Group Package Model update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelUpdate({1}): {2}", svcName, oReq.ModelGroupModel.Name, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupModelGetResp ModelGroupModelGet(ModelGroupModelGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs.Count : 0);
            ModelGroupModelGetResp oRp = new ModelGroupModelGetResp();
            try
            {
                oRp.ModelGroupModels = OnlineRegAdmin.ModelGroupModelGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }
        public ModelGroupModelFindResp ModelGroupModelFind(ModelGroupModelFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelFind({1})", svcName, oReq.ModelGroupModelFind);
            ModelGroupModelFindResp oRp = new ModelGroupModelFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelGroupModelFind(oReq.ModelGroupModelFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelFind({1}): {2}", svcName, oReq.ModelGroupModelFind.ModelGroupModel.ID, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Model Part No

        public ModelPartNoCreateResp ModelPartNoCreate(ModelPartNoCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoCreate({1})", svcName, (oReq != null) ? oReq.ModelPartNo.PartNo : "NULL");
            ModelPartNoCreateResp resp = new ModelPartNoCreateResp();
            try
            {
                resp.ID = OnlineRegAdmin.ModelPartNoCreate(oReq.ModelPartNo);
            }
            catch (Exception ex)
            {
                resp.ID = 0;
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelPartNoCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.ModelPartNo.PartNo : "NULL", resp.ID, resp.Code);
            }
            return resp;
        }
        public ModelPartNoUpdateResp ModelPartNoUpdate(ModelPartNoUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoUpdate({1})", svcName, (oReq != null) ? oReq.ModelPartNo.PartNo : "NULL");
            ModelPartNoUpdateResp resp = new ModelPartNoUpdateResp();
            var count = 0;
            try
            {
                count += OnlineRegAdmin.ModelPartNoUpdate(oReq.ModelPartNo);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelPartNoUpdate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.ModelPartNo.PartNo : "NULL", resp.Code, count);
            }
            return resp;
        }
        public ModelPartNoGetResp ModelPartNoGet(ModelPartNoGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoGet({1})", svcName, (oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0].ToString() : "NULL");
            ModelPartNoGetResp resp = new ModelPartNoGetResp();
            try
            {
                resp.ModelPartNos = OnlineRegAdmin.ModelPartNoGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.ModelPartNoGet({1})", svcName, (oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0].ToString() : "NULL");
            }
            return resp;
        }
        public ModelPartNoFindResp ModelPartNoFind(ModelPartNoFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelPartNoFind({1})", svcName, (oReq != null) ? oReq.ModelPartNoFind.ModelPartNo.PartNo : "NULL");
            ModelPartNoFindResp resp = new ModelPartNoFindResp();
            try
            {
                resp.IDs = OnlineRegAdmin.ModelPartNoFind(oReq.ModelPartNoFind);
            }
            catch (Exception ex)
            {
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                resp.Code = "-1";
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelPartNoFind({1}): {2}|{3}", svcName, (oReq != null) ? oReq.ModelPartNoFind.ModelPartNo.PartNo : "NULL", resp.Code, resp.IDs.Count());
            }
            return resp;
        }

        #endregion

        #region Item Price
        public ItemPriceCreateResp ItemPriceCreate(ItemPriceCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceCreate({1})", svcName, oReq.ItemPrice.BdlPkgID);
            ItemPriceCreateResp oRp = new ItemPriceCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.ItemPriceCreate(oReq.ItemPrice);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ItemPriceCreate({1}): {2}|{3}", svcName, oReq.ItemPrice.BdlPkgID, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public ItemPriceUpdateResp ItemPriceUpdate(ItemPriceUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceUpdate({1})", svcName, oReq.ItemPrice.BdlPkgID);
            ItemPriceUpdateResp oRp = new ItemPriceUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.ItemPriceUpdate(oReq.ItemPrice);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "ItemPrice update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ItemPriceUpdate({1}): {2}", svcName, oReq.ItemPrice.ID, oRp.Code);
            }
            return oRp;
        }
        public ItemPriceGetResp ItemPriceGet(ItemPriceGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            ItemPriceGetResp oRp = new ItemPriceGetResp();
            try
            {
                oRp.ItemPrices = OnlineRegAdmin.ItemPriceGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ItemPriceGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ItemPriceFindResp ItemPriceFind(ItemPriceFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceFind({1})", svcName, oReq.ItemPriceFind);
            ItemPriceFindResp oRp = new ItemPriceFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ItemPriceFind(oReq.ItemPriceFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ItemPriceFind({1}): {2}", svcName, oReq.ItemPriceFind.ItemPrice.BdlPkgID, oRp.Code);
            }
            return oRp;
        }

        public ItemPriceFindResp ItemPriceFindForMultipleContratsAndPlans(ItemPriceFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.ItemPriceFind({1})", svcName, "ItemPriceFindForMultipleContratsAndPlans");
            ItemPriceFindResp oRp = new ItemPriceFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ItemPriceFindForMultipleContratsAndPlans(oReq.ItemPriceFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ItemPriceFind({1}): {2}", svcName, oReq.ItemPriceFind.ItemPrice.BdlPkgID, oRp.Code);
            }
            return oRp;
        }

        #endregion

        //added by Deepika

        #region AdvDepositPrices
        public AdvDepositPriceGetResp AdvDepositPriceGet(AdvDepositPriceGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AdvDepositPriceGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            AdvDepositPriceGetResp oRp = new AdvDepositPriceGetResp();
            try
            {
                oRp.AdvDepositPrices = OnlineRegAdmin.AdvDepositPricesGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AdvDepositPriceGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public AdvDepositPriceGetResp AdvDepositPriceGetBySP(AdvDepositPriceGetReqStr oReq)
        {
            Logger.InfoFormat("Entering {0}.AdvDepositPriceGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : "0");
            AdvDepositPriceGetResp oRp = new AdvDepositPriceGetResp();
            try
            {
                oRp.AdvDepositPrices = OnlineRegAdmin.AdvDepositPricesGetBySP(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AdvDepositPriceGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : "0", oRp.Code);
            }
            return oRp;
        }

        #endregion
        //end Deepika

        #region Property
        //public PropertyCreateResp PropertyCreate(PropertyCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PropertyCreate({1})", svcName, oReq.Property.Name);
        //    PropertyCreateResp oRp = new PropertyCreateResp();
        //    try
        //    {
        //        oRp.ID = OnlineRegAdmin.PropertyCreate(oReq.Property);
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        oRp.ID = 0;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PropertyCreate({1}): {2}|{3}", svcName, oReq.Property.Name, oRp.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        //public PropertyUpdateResp PropertyUpdate(PropertyUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PropertyUpdate({1})", svcName, oReq.Property.Name);
        //    PropertyUpdateResp oRp = new PropertyUpdateResp();
        //    int value = 0;
        //    try
        //    {
        //        value = OnlineRegAdmin.PropertyUpdate(oReq.Property);
        //        if (value == 0)
        //        {
        //            oRp.Code = "-1";
        //            oRp.Message = "Property update fail";

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PropertyUpdate({1}): {2}", svcName, oReq.Property.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        public PropertyGetResp PropertyGet(PropertyGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            PropertyGetResp oRp = new PropertyGetResp();
            try
            {
                oRp.Properties = OnlineRegAdmin.PropertyGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public PropertyGetResp PropertyList()
        {
            Logger.InfoFormat("Entering {0}.PropertyList({1})", svcName, "");
            PropertyGetResp oRp = new PropertyGetResp();
            try
            {
                oRp.Properties = OnlineRegAdmin.PropertyList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PropertyList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public PropertyFindResp PropertyFind(PropertyFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyFind({1})", svcName, oReq.PropertyFind);
            PropertyFindResp oRp = new PropertyFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PropertyFind(oReq.PropertyFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PropertyFind({1}): {2}", svcName, oReq.PropertyFind.Property.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Property Value
        //public PropertyValueCreateResp PropertyValueCreate(PropertyValueCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PropertyValueCreate({1})", svcName, oReq.PropertyValue.Name);
        //    PropertyValueCreateResp oRp = new PropertyValueCreateResp();
        //    try
        //    {
        //        oRp.ID = OnlineRegAdmin.PropertyValueCreate(oReq.PropertyValue);
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        oRp.ID = 0;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PropertyValueCreate({1}): {2}|{3}", svcName, oReq.PropertyValue.Name, oRp.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        //public PropertyValueUpdateResp PropertyValueUpdate(PropertyValueUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PropertyValueUpdate({1})", svcName, oReq.PropertyValue.Name);
        //    PropertyValueUpdateResp oRp = new PropertyValueUpdateResp();
        //    int value = 0;
        //    try
        //    {
        //        value = OnlineRegAdmin.PropertyValueUpdate(oReq.PropertyValue);
        //        if (value == 0)
        //        {
        //            oRp.Code = "-1";
        //            oRp.Message = "PropertyValue update fail";

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PropertyValueUpdate({1}): {2}", svcName, oReq.PropertyValue.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        public PropertyValueGetResp PropertyValueGet(PropertyValueGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyValueGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            PropertyValueGetResp oRp = new PropertyValueGetResp();
            try
            {
                oRp.PropertyValues = OnlineRegAdmin.PropertyValueGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PropertyValueGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public PropertyValueFindResp PropertyValueFind(PropertyValueFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PropertyValueFind({1})", svcName, oReq.PropertyValueFind);
            PropertyValueFindResp oRp = new PropertyValueFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PropertyValueFind(oReq.PropertyValueFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PropertyValueFind({1}): {2}", svcName, oReq.PropertyValueFind.PropertyValue.Name, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region PlanProperty
        //public PlanPropertyCreateResp PlanPropertyCreate(PlanPropertyCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PlanPropertyCreate({1})", svcName, oReq.PlanProperty.Name);
        //    PlanPropertyCreateResp oRp = new PlanPropertyCreateResp();
        //    try
        //    {
        //        oRp.ID = OnlineRegAdmin.PlanPropertyCreate(oReq.PlanProperty);
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        oRp.ID = 0;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PlanPropertyCreate({1}): {2}|{3}", svcName, oReq.PlanProperty.Name, oRp.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        //public PlanPropertyUpdateResp PlanPropertyUpdate(PlanPropertyUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.PlanPropertyUpdate({1})", svcName, oReq.PlanProperty.Name);
        //    PlanPropertyUpdateResp oRp = new PlanPropertyUpdateResp();
        //    int value = 0;
        //    try
        //    {
        //        value = OnlineRegAdmin.PlanPropertyUpdate(oReq.PlanProperty);
        //        if (value == 0)
        //        {
        //            oRp.Code = "-1";
        //            oRp.Message = "PlanProperty update fail";

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.PlanPropertyUpdate({1}): {2}", svcName, oReq.PlanProperty.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        public PlanPropertyGetResp PlanPropertyGet(PlanPropertyGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PlanPropertyGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            PlanPropertyGetResp oRp = new PlanPropertyGetResp();
            try
            {
                oRp.PlanProperties = OnlineRegAdmin.PlanPropertyGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public PlanPropertyFindResp PlanPropertyFind(PlanPropertyFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PlanPropertyFind({1})", svcName, oReq.PlanPropertyFind);
            PlanPropertyFindResp oRp = new PlanPropertyFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.PlanPropertyFind(oReq.PlanPropertyFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PlanPropertyFind({1}): {2}", svcName, oReq.PlanPropertyFind.PlanProperty.BdlPkgID, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region DeviceProperty
        //public DevicePropertyCreateResp DevicePropertyCreate(DevicePropertyCreateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.DevicePropertyCreate({1})", svcName, oReq.DeviceProperty.Name);
        //    DevicePropertyCreateResp oRp = new DevicePropertyCreateResp();
        //    try
        //    {
        //        oRp.ID = OnlineRegAdmin.DevicePropertyCreate(oReq.DeviceProperty);
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        oRp.ID = 0;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.DevicePropertyCreate({1}): {2}|{3}", svcName, oReq.DeviceProperty.Name, oRp.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        //public DevicePropertyUpdateResp DevicePropertyUpdate(DevicePropertyUpdateReq oReq)
        //{
        //    Logger.InfoFormat("Entering {0}.DevicePropertyUpdate({1})", svcName, oReq.DeviceProperty.Name);
        //    DevicePropertyUpdateResp oRp = new DevicePropertyUpdateResp();
        //    int value = 0;
        //    try
        //    {
        //        value = OnlineRegAdmin.DevicePropertyUpdate(oReq.DeviceProperty);
        //        if (value == 0)
        //        {
        //            oRp.Code = "-1";
        //            oRp.Message = "DeviceProperty update fail";

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        oRp.Code = "-1";
        //        //oRp.Message = ex.Message;
        //        //oRp.StackTrace = ex.StackTrace;

        //        string errmsg = "", stacktrace = "";
        //        //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
        //        oRp.Message = errmsg;
        //        oRp.StackTrace = stacktrace;
        //        Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

        //    }
        //    finally
        //    {
        //        Logger.InfoFormat("Exiting {0}.DevicePropertyUpdate({1}): {2}", svcName, oReq.DeviceProperty.ID, oRp.Code);
        //    }
        //    return oRp;
        //}
        public DevicePropertyGetResp DevicePropertyGet(DevicePropertyGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.DevicePropertyGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            DevicePropertyGetResp oRp = new DevicePropertyGetResp();
            try
            {
                oRp.DeviceProperties = OnlineRegAdmin.DevicePropertyGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DevicePropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public DevicePropertyFindResp DevicePropertyFind(DevicePropertyFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.DevicePropertyFind({1})", svcName, oReq.DevicePropertyFind);
            DevicePropertyFindResp oRp = new DevicePropertyFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.DevicePropertyFind(oReq.DevicePropertyFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DevicePropertyFind({1}): {2}", svcName, oReq.DevicePropertyFind.DeviceProperty.ModelID, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region BrandArticle

        public ArticleIDGetResp GetArticleID(int oReq)
        {
            ArticleIDGetResp oRp = new ArticleIDGetResp();
            try
            {
                oRp.ArticleIDs = OnlineRegAdmin.GetArticleID(oReq.ToInt());
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ArticleIDGetRespSmart GetArticleIDSmart(int oReq)
        {
            ArticleIDGetRespSmart oRp = new ArticleIDGetRespSmart();
            try
            {
                oRp.ArticleIDs = OnlineRegAdmin.GetArticleIDSmart(oReq.ToInt());
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ArticleIDGetResp GetArticleIDs(List<int> oReq)
        {
            ArticleIDGetResp oRp = new ArticleIDGetResp();
            try
            {
                oRp.ArticleIDs = OnlineRegAdmin.GetArticleIDs(oReq);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ArticleIDGetResp GetAllArticleIDs()
        {
            ArticleIDGetResp oRp = new ArticleIDGetResp();
            try
            {
                oRp.ArticleIDs = OnlineRegAdmin.GetAllArticleIDs();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public ArticleIDGetRespSmart GetArticleIDsSmart(List<int> oReq)
        {
            ArticleIDGetRespSmart oRp = new ArticleIDGetRespSmart();
            try
            {
                oRp.ArticleIDs = OnlineRegAdmin.GetArticleIDsSmart(oReq);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public BrandArticleImageGetResp BrandArticleModelImageGet(BrandArticleModelImageGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandArticleModelImageGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            BrandArticleImageGetResp oRp = new BrandArticleImageGetResp();
            try
            {
                oRp.BarndArticleImages = OnlineRegAdmin.BrandArticleImageGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandArticleModelImageGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public BrandArticleImageGetRespSmart BrandArticleModelImageGetSmart(BrandArticleModelImageGetReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.BrandArticleModelImageGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            BrandArticleImageGetRespSmart oRp = new BrandArticleImageGetRespSmart();
            try
            {
                oRp.BarndArticleImages = OnlineRegAdmin.BrandArticleImageGetSmart(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.BrandArticleModelImageGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public int GetContractDuration(int Id)
        {

            int oRp = 0;
            try
            {
                oRp = OnlineRegAdmin.GetContractDuration(Id);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        public string GetUomId(string ArticleId, int ContractDuration, string contractID, string offerName)
        {
            string oRp = string.Empty;
            try
            {
                oRp = OnlineRegAdmin.GetUomId(ArticleId, ContractDuration, contractID, offerName);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }


        #endregion

        #region Implementaion of GetIMOPSDetails

        //public IMPOSDetailsResp GetIMOPSDetails(int RegID)
        //{
        //    IMPOSDetails objIMPOSDetails = null;
        //    IMPOSDetailsResp oblIMPOSDetailsResp = new IMPOSDetailsResp();
        //    try
        //    {
        //        objIMPOSDetails = OnlineRegAdmin.getIMPOSDetails(RegID);
        //        if (objIMPOSDetails != null)
        //        {

        //            oblIMPOSDetailsResp.RegID = RegID;
        //            oblIMPOSDetailsResp.ArticleID = objIMPOSDetails.ArticleID;
        //            oblIMPOSDetailsResp.MSISDN1 = objIMPOSDetails.MSISDN1;
        //            oblIMPOSDetailsResp.KenanAccountNo = objIMPOSDetails.KenanAccountNo;
        //            oblIMPOSDetailsResp.FullName = objIMPOSDetails.FullName;
        //            oblIMPOSDetailsResp.IDCardNo = objIMPOSDetails.IDCardNo;
        //            oblIMPOSDetailsResp.Line1 = objIMPOSDetails.Line1;
        //            oblIMPOSDetailsResp.Line2 = objIMPOSDetails.Line2;
        //            oblIMPOSDetailsResp.Line3 = objIMPOSDetails.Line3;
        //            oblIMPOSDetailsResp.Town = objIMPOSDetails.Town;
        //            oblIMPOSDetailsResp.Postcode = objIMPOSDetails.Postcode;
        //            oblIMPOSDetailsResp.IMEINumber = objIMPOSDetails.IMEINumber;
        //            oblIMPOSDetailsResp.ModelID = objIMPOSDetails.ModelID;
        //            oblIMPOSDetailsResp.SimNo = objIMPOSDetails.SimNo;
        //            if (objIMPOSDetails.UOMCODE == null)
        //            {
        //                oblIMPOSDetailsResp.UOMCODE = 0.ToString();

        //            }
        //            else
        //                oblIMPOSDetailsResp.UOMCODE = objIMPOSDetails.UOMCODE;

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
        //    }
        //    return oblIMPOSDetailsResp;
        //}
        #endregion

        #region Implementaion of GetIMOPSDetails

        public WhiteListDetailsResp getWhilstDescreption(string UserICNO)
        {
            WhiteListDetails objWhiteListDetails = null;
            WhiteListDetailsResp oblWhiteListDetailsResp = new WhiteListDetailsResp();
            try
            {
                objWhiteListDetails = OnlineRegAdmin.getWhilstDescreption(UserICNO);
                if (objWhiteListDetails != null)
                {

                    oblWhiteListDetailsResp.Description = objWhiteListDetails.Description;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oblWhiteListDetailsResp;
        }
        #endregion

        #region Added by VLT for Sim Model on 09 Apr 2013
        public SimModelTypeGetResp getSIMModels()
        {
            Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            SimModelTypeGetResp oRp = new SimModelTypeGetResp();
            List<SimModels> value = null;
            try
            {
                value = OnlineRegAdmin.SimModelTypeGet();
                oRp.SimModels = value;
            }
            catch (Exception ex)
            {
                //oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
                value = null;
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.ModelGet({1}): {2}", svcName, oReq == null || oReq.IDs == null || oReq.IDs.Count() == 0 ? 0 : oReq.IDs[0], oRp.Code);
                Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            }
            return oRp;
        }
        #endregion

        #region Added by VLT for Sim Replacement on 17 June 2013
        public SimModels GetSimModelDetails(int id, int articalId)
        {
            Logger.InfoFormat("Entering {0}.GetSimModelDetails({1})", svcName, "");
            SimModelTypeGetResp oRp = new SimModelTypeGetResp();
            SimModels value = null;
            try
            {
                value = OnlineRegAdmin.GetSimModelDetails(id, articalId);
                //oRp.SimModels = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                value = null;
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.GetSimModelDetails({1})", svcName, "");
            }
            return value;
        }
        public SimReplacementReasonGetResp GetSimReplacementReason(bool accType)
        {
            Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            SimReplacementReasonGetResp oRp = new SimReplacementReasonGetResp();
            List<SimReplacementReasons> value = null;
            try
            {
                value = OnlineRegAdmin.SimReplacementReasonTypeGet(accType);
                oRp.ReplacementReasons = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                value = null;
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            }
            return oRp;
        }


        public SimReplacementReasonResp GetSimReplacementReasonDetails(int ReasonId)
        {
            Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            SimReplacementReasonResp oRp = new SimReplacementReasonResp();
            SimReplacementReasons value = new SimReplacementReasons(); ;
            try
            {
                value = OnlineRegAdmin.GetSimReplacementReasonDetails(ReasonId);
                oRp.ReplacementReasonsDetails = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                value = null;
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.ModelGet({1})", svcName, "");
            }
            return oRp;
        }

        #endregion

        #region GetOfferID
        public List<int> GetOfferID(string ArticleId)
        {
            List<int> oRp = new List<int>();
            try
            {
                oRp = OnlineRegAdmin.GetOfferID(ArticleId);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        #endregion

        public MOCOfferDetailsRes GetMOCOfferDetails(int OfferID)
        {
            MOCOfferDetails objMOCOfferDetails = null;
            MOCOfferDetailsRes oblMOCOfferDetailsRes = new MOCOfferDetailsRes();
            try
            {
                objMOCOfferDetails = OnlineRegAdmin.getMOCOfferDetails(OfferID);
                if (objMOCOfferDetails != null)
                {

                    oblMOCOfferDetailsRes.ID = objMOCOfferDetails.ID;
                    oblMOCOfferDetailsRes.Name = objMOCOfferDetails.Name;
                    oblMOCOfferDetailsRes.OfferDescription = objMOCOfferDetails.OfferDescription;
                    oblMOCOfferDetailsRes.DiscountAmount = objMOCOfferDetails.DiscountAmount;



                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oblMOCOfferDetailsRes;
        }


        #region"Get offer Name by Id by VLT"
        /// <summary>
        /// Method to Get Offer Name
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public string GetOfferNameById(int offerID)
        {
            string offerName = string.Empty;
            try
            {
                offerName = OnlineRegAdmin.GetOfferNameById(offerID);
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return offerName;
        }
        #endregion

        public List<int> GetAllDependentComponents(int componentId, int planId)
        {
            List<int> depCompIds = new List<int>();
            try
            {
                depCompIds = OnlineRegAdmin.GetAllDependentcomponents(componentId, planId);

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return depCompIds;
        }

        public List<int> GetAllSmartDependentComponents(int componentId, int planId)
        {
            List<int> depCompIds = new List<int>();
            try
            {
                depCompIds = OnlineRegAdmin.GetAllSmartDependentcomponents(componentId, planId);

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return depCompIds;
        }

        public List<int> GetAllDependentComponentsList(List<int> componentId, int planId)
        {
            List<int> depCompIds = new List<int>();
            try
            {
                depCompIds = OnlineRegAdmin.GetAllDependentcomponents(componentId, planId);

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return depCompIds;
        }

        public lnkofferuomarticlepriceResp GetOffersfromUOMArticle(string ArticleId, string planID, string contractID, string MOCStatus)
        {
            lnkofferuomarticlepriceResp value = new lnkofferuomarticlepriceResp();
            try
            {
                value.values = OnlineRegAdmin.GetOffersfromUOMArticle(ArticleId, planID, contractID, MOCStatus);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return value;
        }


        public lnkofferuomarticlepriceResp GetOffersfromUOMArticleNew(string ArticleId, string PkgKenanID, string kenanIds, string MOCStatus, int AcctCtg, int MktCode)
        {
            lnkofferuomarticlepriceResp value = new lnkofferuomarticlepriceResp();
            try
            {
                value.values = OnlineRegAdmin.GetOffersfromUOMArticleNew(ArticleId, PkgKenanID, kenanIds, MOCStatus, AcctCtg, MktCode);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return value;
        }


        public lnkofferuomarticlepriceResp GetOffersfromUOMArticle2(string ArticleId, string PackageKenanID, string MOCStatus, string contractKenanIDs)
        {
            lnkofferuomarticlepriceResp value = new lnkofferuomarticlepriceResp();
            try
            {
                value.values = OnlineRegAdmin.GetOffersfromUOMArticle2(ArticleId, PackageKenanID, MOCStatus, contractKenanIDs);
            }
            catch (Exception ex)
            { Logger.Error(" Error: " + Util.LogException(ex)); }
            return value;
        }

        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        public List<string> GetContractsForArticleId(string articleId, string planId, string mocStatus)
        {
            List<string> objContracts = new List<string>();
            try
            {
                objContracts = OnlineRegAdmin.GetContractsForArticleId(articleId, planId, mocStatus);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return objContracts;
        }
        public List<string> GetPackagesForArticleId(string articleId, string mocStatus)
        {
            List<string> objContracts = new List<string>();
            try
            {
                objContracts = OnlineRegAdmin.GetPackagesForArticleId(articleId, mocStatus);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return objContracts;
        }
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid


        //Added by VLT on 17 May 2013 to get device image details dynamically
        public GetDeviceImageDetailsResp GetDeviceImageDetails(string articleId, int modelId)
        {

            GetDeviceImageDetailsResp objDeviceDetails = new GetDeviceImageDetailsResp();
            try
            {
                objDeviceDetails.deviceDetails = OnlineRegAdmin.GetDeviceImageDetails(articleId, modelId);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return objDeviceDetails;

        }
        public GetDeviceImageDetailsRespSmart GetDeviceImageDetailsForSmart(string articleId, int modelId)
        {

            GetDeviceImageDetailsRespSmart objDeviceDetails = new GetDeviceImageDetailsRespSmart();
            try
            {
                objDeviceDetails.deviceDetails = OnlineRegAdmin.GetDeviceImageDetailsForSmart(articleId, modelId);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return objDeviceDetails;

        }
        //Added by VLT on 17 May 2013 to get device image details dynamically


        //Added by VLT on 17 May 2013 to get device image details dynamically

        public BrandArticleGetResp BrandArticleGet(string articleId)
        {
            BrandArticleGetResp value = new BrandArticleGetResp();
            try
            {
                value.brandArticle = OnlineRegAdmin.BrandArticleGet(articleId);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return value;
        }

        public DiscountPricesRes GetDiscountPrices(int Modelid, int planid, string contractid, int dataplanid)
        {
            DiscountPriceDetail objDisocuntOfferDetails = null;
            DiscountPricesRes oblDiscountOfferDetailsRes = new DiscountPricesRes();
            try
            {
                objDisocuntOfferDetails = OnlineRegAdmin.getDiscountPriceDetails(Modelid, planid, contractid, dataplanid);
                if (objDisocuntOfferDetails != null)
                {

                    oblDiscountOfferDetailsRes.ID = objDisocuntOfferDetails.ID;
                    oblDiscountOfferDetailsRes.NewPlanName = objDisocuntOfferDetails.NewPlanName;
                    oblDiscountOfferDetailsRes.OrgPrice = objDisocuntOfferDetails.OrgPrice;
                    oblDiscountOfferDetailsRes.DiscountPrice = objDisocuntOfferDetails.DiscountPrice;
                    oblDiscountOfferDetailsRes.ContractID = objDisocuntOfferDetails.ContractID;
                    oblDiscountOfferDetailsRes.DataPlanID = objDisocuntOfferDetails.DataPlanID;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oblDiscountOfferDetailsRes;
        }



        #region"GetContractIdByRegID BY Id by VLT"
        /// <summary>
        /// Method to GetContractIdByRegID
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public List<int> GetContractIdByRegID(int regID)
        {
            List<int> contractId = new List<int>();
            try
            {
                contractId = OnlineRegAdmin.GetContractByRegId(regID).ToList();
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractId;
        }
        public List<int> GetContractByRegIdForSmart(int regID)
        {
            List<int> contractId = new List<int>();
            try
            {
                contractId = OnlineRegAdmin.GetContractByRegIdForSmart(regID).ToList();
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractId;
        }
        public int GetContractIdByRegIDSeco(int regID)
        {
            int contractId = 0;
            try
            {
                contractId = OnlineRegAdmin.GetContractByRegIdSeco(regID);
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractId;
        }
        public int GetContractIdByRegSuppLineID(int regSuppLineID)
        {
            int contractId = 0;
            try
            {
                contractId = OnlineRegAdmin.GetContractByRegSuppLineId(regSuppLineID);
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractId;
        }
        #endregion


        #region"GetContractByCode BY Id by VLT" Added by VLT 06 May 2013
        /// <summary>
        /// Method to GetContractByCode
        /// </summary>
        /// <param name="offerID"></param>
        /// <returns>string</returns>
        public string GetContractByCode(string compCode)
        {
            string contractValue = "";
            try
            {
                contractValue = OnlineRegAdmin.GetContractByCode(compCode);
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractValue;
        }
        #endregion

        #region "Add Remove VAS"

        public long SaveAddRemoveVAS(AddRemoveVASReq oReq)
        {
            Logger.InfoFormat("Entering {0}.SaveAddRemoveVAS()", svcName);

            var resp = new LnkRegSpendLimitReq();
            long outValue;

            try
            {
                outValue = DAL.Admin.OnlineRegAdmin.SaveAddRemoveVAS(oReq.AddRemoveVASComponents);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveAddRemoveVAS()", svcName);
            }

            return outValue;
        }


        public int UpdateAddRemoveVAS(AddRemoveVASReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UpdateAddRemoveVAS()", svcName);

            var resp = new LnkRegSpendLimitReq();
            int outValue;

            try
            {
                outValue = DAL.Admin.OnlineRegAdmin.UpdateAddRemoveVASComponent(oReq.AddRemoveVASComponents);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UpdateAddRemoveVAS()", svcName);
            }

            return outValue;
        }

        public int DeleteAddRemoveVasComponent(int Id)
        {
            Logger.InfoFormat("Entering {0}.DeleteAddRemoveVasComponent({1})", svcName);

            int outValue;

            try
            {
                outValue = DAL.Admin.OnlineRegAdmin.DeleteAddRemoveVasComponent(Id);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DeleteAddRemoveVasComponent()", svcName);
            }

            return outValue;
        }

        public int DeleteAddRemoveVasComponents(int regId)
        {
            Logger.InfoFormat("Entering {0}.DeleteAddRemoveVasComponents()", svcName);

            int outValue = 0;

            try
            {
                DAL.Admin.OnlineRegAdmin.DeleteAddRemoveVASComponents(regId);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";
                outValue = -1;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.DeleteAddRemoveVasComponents()", svcName);
            }

            return outValue;


        }
        public AddRemoveVASResp GetAddRemoveVASComponents(int regId)
        {
            Logger.InfoFormat("Entering {0}.SaveExtraTenLogs()", svcName);
            AddRemoveVASResp response = new AddRemoveVASResp();
            try
            {
                response.AddRemoveVASComponents = DAL.Admin.OnlineRegAdmin.GetAddRemoveVASComponents(regId);
            }
            catch (Exception ex)
            {
                // resp.Code = "-1";

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace:{1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveExtraTenLogs()", svcName);
            }

            return response;

        }

        #endregion

        public List<RegSmartComponents> GetSelectedComponents(string regId)
        {
            RegSmartComponentsResp resp = new RegSmartComponentsResp();
            try
            {
                resp.regsmartComponents = OnlineRegAdmin.GetSelectedComponents(regId);

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }

            return resp.regsmartComponents;

        }

        public List<RegSmartComponents> GetSelectedComponentsForCRPPlan(string regId)
        {
            RegSmartComponentsResp resp = new RegSmartComponentsResp();
            try
            {
                resp.regsmartComponents = OnlineRegAdmin.GetSelectedComponentsForCRPPlan(regId);

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }

            return resp.regsmartComponents;

        }

        public PgmBdlPckComponentGetResp PgmBdlPckComponentGetForSmartByKenanCode(List<string> oReq)
        {

            PgmBdlPckComponentGetResp oRp = new PgmBdlPckComponentGetResp();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.ProgramBundlePackageComponentGetForSmartByKenanCode(oReq);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return oRp;
        }

        public List<ModelPackageInfo> GetModelPackageInfo_smart(int ModelId, int packageId, string KenanCode,bool isSmart=false)
        {

            List<ModelPackageInfo> oRp = new List<ModelPackageInfo>();
            try
            {
                oRp = OnlineRegAdmin.GetModelPackageInfo_smart(ModelId, packageId, KenanCode, isSmart);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }

            return oRp;
        }


        public ExtendBrandFindResp ExtendBrand(ExtendBrandFindReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, oReq.BrandFind);
            ExtendBrandFindResp oRp = new ExtendBrandFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ExtendBrand(oReq.ContractBundleModels);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                //Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, oReq.BrandFind.Brand.Name, oRp.Code);
            }
            return oRp;
        }
        public List<Brand> GetCRPBrands(string type)
        {
            List<Brand> objBrandExt = null;
            try
            {
                objBrandExt = OnlineRegAdmin.BrandGetCRP(type);
                if (objBrandExt != null)
                {
                    return objBrandExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objBrandExt;

        }


        public List<Model> GetCrpModels(int Brandid, string type)
        {
            List<Model> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.ModelCRPGet(Brandid, type);
                if (objModelExt != null)
                {
                    return objModelExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objModelExt;

        }



        public List<ModelGroup> GetCrpPackages(int Modelid, string type)
        {
            List<ModelGroup> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.PackagesCRPGet(Modelid, type);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }



        public List<int> GetExtendedPackages(int Modelid)
        {
            List<int> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetExtendedPackages(Modelid);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }

        public List<DAL.Models.ContractDataplan> GetContractDataPlan(int DataPlanID)
        {
            List<DAL.Models.ContractDataplan> objPackagesExt1 = null;
            try
            {
                objPackagesExt1 = OnlineRegAdmin.GetContractDataPlan(DataPlanID);
                if (objPackagesExt1 != null)
                {
                    return objPackagesExt1;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt1;

        }



        public List<Model> GetExtendedModelsDevice(int brandid, string type)
        {
            List<Model> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetExtendedModelsDevice(brandid, type);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }



        public ExtendPlanFindResp ExtendPlan(ExtendPlanFindReq oReq)
        {
            //Logger.InfoFormat("Entering {0}.BrandFind({1})", svcName, oReq.BrandFind);
            ExtendPlanFindResp oRp = new ExtendPlanFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.ExtendPlanBrand(oReq.ContractPlan);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                //Logger.InfoFormat("Exiting {0}.BrandFind({1}): {2}", svcName, oReq.BrandFind.Brand.Name, oRp.Code);
            }
            return oRp;
        }
        #region Implementaion of GetIMOPSDetails

        public CRPIMPOSDetailsResp GetIMOPSDetails(int RegID)
        {
            IMPOSDetails objIMPOSDetails = null;
            CRPIMPOSDetailsResp oblIMPOSDetailsResp = new CRPIMPOSDetailsResp();
            try
            {
                IMPOSDetails res = OnlineRegAdmin.GetPosKeyandOrgID(RegID);

                string OrgnaizationId = "4";  // by default it takes as per existing functionality kept it (Raj)

                if (res != null && !string.IsNullOrEmpty(res.OrganisationId) && res.RegTypeID == 24)
                {
                    OrgnaizationId = res.OrganisationId;
                }
                objIMPOSDetails = OnlineRegAdmin.getIMPOSDetails(RegID, OrgnaizationId, "P");
                if (objIMPOSDetails != null)
                {

                    oblIMPOSDetailsResp.RegID = RegID;
                    oblIMPOSDetailsResp.ArticleID = objIMPOSDetails.ArticleID;
                    oblIMPOSDetailsResp.MSISDN1 = objIMPOSDetails.MSISDN1;
                    oblIMPOSDetailsResp.KenanAccountNo = objIMPOSDetails.KenanAccountNo;
                    oblIMPOSDetailsResp.FullName = objIMPOSDetails.FullName;
                    oblIMPOSDetailsResp.IDCardNo = objIMPOSDetails.IDCardNo;
                    oblIMPOSDetailsResp.Line1 = objIMPOSDetails.Line1;
                    oblIMPOSDetailsResp.Line2 = objIMPOSDetails.Line2;
                    oblIMPOSDetailsResp.Line3 = objIMPOSDetails.Line3;
                    oblIMPOSDetailsResp.Town = objIMPOSDetails.Town;
                    oblIMPOSDetailsResp.Postcode = objIMPOSDetails.Postcode;
                    oblIMPOSDetailsResp.IMEINumber = objIMPOSDetails.IMEINumber;
                    oblIMPOSDetailsResp.ModelID = objIMPOSDetails.ModelID;
                    oblIMPOSDetailsResp.SimNo = objIMPOSDetails.SimNo;
                    if (objIMPOSDetails.UOMCODE == null)
                    {
                        oblIMPOSDetailsResp.UOMCODE = 0.ToString();

                    }
                    else
                        oblIMPOSDetailsResp.UOMCODE = objIMPOSDetails.UOMCODE;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oblIMPOSDetailsResp;
        }

        public List<IMPOSDetails> GetPosKeyandOrgIDNew(int RegID)
        {
            List<IMPOSDetails> lstIMPOSDetails = OnlineRegAdmin.GetPosKeyandOrgIDNew(RegID);
            return lstIMPOSDetails;
        }
        public IMPOSSpecDetails GetIMPOSSpecDetails(string PosKey)
        {
            IMPOSSpecDetails objIMPOSSpecDetails = OnlineRegAdmin.getIMPOSSpecDetails(PosKey);
            return objIMPOSSpecDetails;
        }
        public IMPOSDetails getIMPOSDetails_Smart(int RegID, string OrganisationId, string strSIMType)
        {
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            objIMPOSDetail = OnlineRegAdmin.getIMPOSDetails_Smart(RegID, OrganisationId, strSIMType);
            return objIMPOSDetail;
        }
        public int GetArticleIdById(int SIMId)
        {
            int value = 0;
            value = OnlineRegAdmin.GetArticleIdById(SIMId);

            return value;
        }

        public int UpdateIMPOSDetails(int RegID, string IMPOSFileName, int IMPOSStatus, string strSIMType = "P")
        {
            int isUpdated = -1;
            isUpdated = OnlineRegAdmin.UpdateIMPOSDetails(RegID, IMPOSFileName, IMPOSStatus, strSIMType);

            return isUpdated;
        }
        public IMPOSDetails GetIMPOSDetails(int RegID, string OrganisationId, string strSIMType)
        {
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            objIMPOSDetail = OnlineRegAdmin.getIMPOSDetails(RegID,OrganisationId,strSIMType);

            return objIMPOSDetail;
        }
        public string GetUOMCodeBySIMModelID(int SIMId)
        {
            string value = string.Empty;
            value = OnlineRegAdmin.GetUOMCodeBySIMModelID(SIMId);

            return value;
        }
        public IMPOSDetails getIMPOSDetailsForPlanOnly(int RegID, string OrganisationId, string strSIMType = "P")
        {
            IMPOSDetails objIMPOSDetail = new IMPOSDetails();
            objIMPOSDetail = OnlineRegAdmin.getIMPOSDetailsForPlanOnly(RegID, OrganisationId, strSIMType);

            return objIMPOSDetail;
        }
        #endregion
        #region Implementation of Waiver Componets

        #endregion

        #region AddContract

        public List<Brand> GetBrandsForPackage(string packageId,string planType)
        {
            List<Brand> objBrandExt = null;
            try
            {
                objBrandExt = OnlineRegAdmin.GetBrandsForPackage(packageId,planType);
                if (objBrandExt != null)
                {
                    return objBrandExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
            }
            return objBrandExt;

        }

        public List<Model> GetModelsForPackage(int Brandid, string packageId, string planType)
        {
            List<Model> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetDevicesForPackage(Brandid, packageId, planType);
                if (objModelExt != null)
                {
                    return objModelExt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
            }
            return objModelExt;

        }

        public List<BrandArticle> GetDeviceListForPackage(string packageId, string mktCode, string acctCategory)
        {
            List<BrandArticle> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetDeviceListForPackage(packageId, mktCode, acctCategory);
                if (objModelExt != null)
                {
                    return objModelExt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
            }
            return objModelExt;

        }

        public List<Model> GetModelsForExtenstionForPackage(int Brandid, string packageId, string planType)
        {
            List<Model> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetModelsForExtenstionForPackage(Brandid, packageId,planType);
                if (objModelExt != null)
                {
                    return objModelExt;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
            }
            return objModelExt;

        }

        #endregion AddContract

        #region Devices for supplines

        public List<Brand> GetBrandsForSuppLines()
        {
            List<Brand> objBrandExt = null;
            try
            {
                objBrandExt = OnlineRegAdmin.GetBrandsForSupplines();
                if (objBrandExt != null)
                {
                    return objBrandExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objBrandExt;

        }

        public List<Model> GetModelsForSupplines(int Brandid)
        {
            List<Model> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetDevicesForSupplines(Brandid);
                if (objModelExt != null)
                {
                    return objModelExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objModelExt;

        }

        public List<Model> GetModelsForExtenstionForSupplines(int Brandid)
        {
            List<Model> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetModelsForExtenstionForSupplines(Brandid);
                if (objModelExt != null)
                {
                    return objModelExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objModelExt;

        }

        public List<ModelGroup> GetModelGroupsForSupplines(int Modelid)
        {
            List<ModelGroup> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetModelGroupsForSupplines(Modelid);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }

        public List<int> GetExtendedPackagesForSupplines(int Modelid)
        {
            List<int> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetExtendedPackagesForSupplines(Modelid);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;
        }


        #endregion

        #region restricted kenan codes

        public RestrictedComponentsResp GetAllRestrictedComponents()
        {

            RestrictedComponentsResp restrctedcomps = new RestrictedComponentsResp();
            try
            {
                restrctedcomps.RestrictedComponents = OnlineRegAdmin.GetAllRestrictedComponents();
                if (restrctedcomps != null)
                {
                    return restrctedcomps;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {

            }
            return restrctedcomps;

        }

        #endregion

        //Changes related to Multiple Contracts CR changed by code--Chetan/ db--Pavan
        public List<PlanPrices> GetPlanPrice(int _modelID, string _uom)
        {
            Logger.InfoFormat("Entering {0}.GetPlanPrice({1},{2})", svcName, _modelID, _uom);

            List<PlanPrices> PlanPrices = new List<PlanPrices>();
            try
            {
                PlanPrices = OnlineRegAdmin.GetPlanPrice(_modelID, _uom);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPlanPrice({1},{2})", svcName, _modelID, _uom);
            }

            return PlanPrices;
        }


        public PgmBdlPckComponentGetResp PgmBdlPckComponentGet2(int componentId, int planId)
        {

            List<int> depCompIds = new List<int>();


            PgmBdlPckComponentGetResp oRp = new PgmBdlPckComponentGetResp();
            try
            {
                depCompIds = OnlineRegAdmin.GetAllDependentcomponents(componentId, planId);
                oRp.PgmBdlPckComponents = OnlineRegAdmin.ProgramBundlePackageComponentGet(depCompIds);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        #region smart related methods

        public IMPOSDetailsResp GetIMOPSDetails_Smart(int RegID)
        {
            IMPOSDetails objIMPOSDetails = null;
            IMPOSDetailsResp oblIMPOSDetailsResp = new IMPOSDetailsResp();
            try
            {
                objIMPOSDetails = OnlineRegAdmin.getIMPOSDetails(RegID);
                if (objIMPOSDetails != null)
                {

                    oblIMPOSDetailsResp.RegID = RegID;
                    oblIMPOSDetailsResp.ArticleID = objIMPOSDetails.ArticleID;
                    oblIMPOSDetailsResp.MSISDN1 = objIMPOSDetails.MSISDN1;
                    oblIMPOSDetailsResp.KenanAccountNo = objIMPOSDetails.KenanAccountNo;
                    oblIMPOSDetailsResp.FullName = objIMPOSDetails.FullName;
                    oblIMPOSDetailsResp.IDCardNo = objIMPOSDetails.IDCardNo;
                    oblIMPOSDetailsResp.Line1 = objIMPOSDetails.Line1;
                    oblIMPOSDetailsResp.Line2 = objIMPOSDetails.Line2;
                    oblIMPOSDetailsResp.Line3 = objIMPOSDetails.Line3;
                    oblIMPOSDetailsResp.Town = objIMPOSDetails.Town;
                    oblIMPOSDetailsResp.Postcode = objIMPOSDetails.Postcode;
                    oblIMPOSDetailsResp.IMEINumber = objIMPOSDetails.IMEINumber;
                    oblIMPOSDetailsResp.ModelID = objIMPOSDetails.ModelID;
                    oblIMPOSDetailsResp.SimNo = objIMPOSDetails.SimNo;
                    if (objIMPOSDetails.UOMCODE == null)
                    {
                        oblIMPOSDetailsResp.UOMCODE = 0.ToString();

                    }
                    else
                        oblIMPOSDetailsResp.UOMCODE = objIMPOSDetails.UOMCODE;

                }
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oblIMPOSDetailsResp;
        }

        public PgmBdlPckComponentFindRespSmart PgmBdlPckComponentFindSmart(PgmBdlPckComponentFindReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentFind({1})", svcName, oReq.PgmBdlPckComponentFindSmart);
            PgmBdlPckComponentFindRespSmart oRp = new PgmBdlPckComponentFindRespSmart();
            try
            {
                oRp.IDs = OnlineRegAdmin.ProgramBundlePackageComponentFindSmart(oReq.PgmBdlPckComponentFindSmart);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentFind({1}): {2}", svcName, oReq.PgmBdlPckComponentFindSmart.PgmBdlPckComponentSmart.ID, oRp.Code);
            }
            return oRp;
        }

        public SmartRebateRes GetSmartRebate(SmartRebateReq oReq)
        {
            SmartRebateRes oRp = new SmartRebateRes();
            try
            {
                oRp.ID = Registration.DAL.Registration.SmartRebateIDGet(oReq.SmartRebates.Rebate);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            return oRp;
        }

        public PgmBdlPckComponentGetRespSmart PgmBdlPckComponentGetForSmart(PgmBdlPckComponentGetReqSmart oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGetForSmart({1})", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0);
            PgmBdlPckComponentGetRespSmart oRp = new PgmBdlPckComponentGetRespSmart();
            try
            {
                oRp.PgmBdlPckComponents = OnlineRegAdmin.ProgramBundlePackageComponentGetForSmart(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGetForSmart({1}): {2}", svcName, (oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public ModelGroupModelGetResp_Smart ModelGroupModelGet_Smart(ModelGroupModelGetReq_Smart oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs.Count : 0);
            ModelGroupModelGetResp_Smart oRp = new ModelGroupModelGetResp_Smart();
            try
            {
                oRp.ModelGroupModels_Smart = OnlineRegAdmin.ModelGroupModelGet_Smart(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count : 0, oRp.Code);
            }
            return oRp;
        }

        public ModelGroupModelFindResp_Smart ModelGroupModelFind_Smart(ModelGroupModelFindReq_Smart oReq)
        {
            Logger.InfoFormat("Entering {0}.ModelGroupModelFind({1})", svcName, oReq.ModelGroupModelFind_Smart);
            ModelGroupModelFindResp_Smart oRp = new ModelGroupModelFindResp_Smart();
            try
            {
                oRp.IDs = OnlineRegAdmin.ModelGroupModelFind_Smart(oReq.ModelGroupModelFind_Smart);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.ModelGroupModelFind({1}): {2}", svcName, oReq.ModelGroupModelFind_Smart.ModelGroupModel.ID, oRp.Code);
            }
            return oRp;
        }

        public List<ModelGroup> GetModelGroupSmartForSupplines(int Modelid)
        {
            List<ModelGroup> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetModelGroupSmartForSupplines(Modelid);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }

        public List<ModelGroup> GetCrpSmartPackages(int Modelid, string type)
        {
            List<ModelGroup> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.PackagesCRPSmartGet(Modelid, type);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }



        public List<int> GetSmartExtendedPackages(int Modelid)
        {
            List<int> objPackagesExt = null;
            try
            {
                objPackagesExt = OnlineRegAdmin.GetSmartExtendedPackages(Modelid);
                if (objPackagesExt != null)
                {
                    return objPackagesExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objPackagesExt;

        }

        #endregion

        public VoiceContractDetailsGetResp GetVoiceAddedendums(VoiceContractDetailsReqStr oReq)
        {
            Logger.InfoFormat("Entering {0}.AdvDepositPriceGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : "0");
            VoiceContractDetailsGetResp oRp = new VoiceContractDetailsGetResp();
            try
            {
                string contractKenanCodes = string.Empty;
                if (oReq.IDs.Count > 0)
                {
                    foreach (string contractKenanCode in oReq.IDs)
                    {
                        if (contractKenanCodes == string.Empty)
                        {
                            contractKenanCodes = contractKenanCode;
                        }
                        else
                        {
                            contractKenanCodes = contractKenanCodes + "," + contractKenanCode;
                        }
                    }
                }
                oRp.lstVoiceContractDetails = OnlineRegAdmin.GetVoiceAddedendums(contractKenanCodes);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AdvDepositPriceGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : "0", oRp.Code);
            }
            return oRp;
        }

        public Dictionary<string, string> RetriveDCGreater2GB()
        {
            Dictionary<string, string> DcComponents = new Dictionary<string, string>();
            try
            {
                Logger.InfoFormat("Entering {0}.RetriveDCGreater2GB()", svcName);
                DcComponents = OnlineRegAdmin.GetDataComponentGreater2GB();
                Logger.InfoFormat("Exiting {0}.RetriveDCGreater2GB()", svcName);
            }
            catch(Exception ex)
            {
                Logger.Error(ex); 
                Logger.InfoFormat("Exiting {0}.RetriveDCGreater2GB()", svcName);
            }
            return DcComponents;
        }

        public Dictionary<string, string> FindPackageOnKenanCode(string KenanCode)
        {
            Dictionary<string, string> pkgDetls = new Dictionary<string, string>();
            try
            {
                Logger.InfoFormat("Entering {0}.FindPackageOnKenanCode({1})", svcName,KenanCode);
                pkgDetls = OnlineRegAdmin.FindPackageOnKenanCode(KenanCode);
                Logger.InfoFormat("Exiting {0}.FindPackageOnKenanCode(1)", svcName,KenanCode);
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                Logger.InfoFormat("Exiting {0}.FindPackageOnKenanCode({1})", svcName,KenanCode);
            }
            return pkgDetls;
        }

        #region Added to get sim model based on article & sim type

        public List<SimModels> GetModelName(int ArticleId, string simCardType)
        {
            List<SimModels> value = new List<SimModels>();
            try
            {
                value = OnlineRegAdmin.GetModelName(ArticleId, simCardType);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return value;
        }
        #endregion

        #region Added to get SIM Types
        public SimCardTypesGetResp GetSIMCardTypes()
        {
            Logger.InfoFormat("Entering {0}.GetSIMTypes({1})", svcName, "");
            SimCardTypesGetResp oRp = new SimCardTypesGetResp();
            List<SIMCardTypes> value = null;
            try
            {
                value = OnlineRegAdmin.GetSIMCardTypes();
                oRp.SIMCardTypes = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                value = null;
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.GetSIMTypes({1})", svcName, "");
            }
            return oRp;
        }
        #endregion
        
        public List<string> GetDataContractByRegId(int regID)
        {
            List<string> contractId = new List<string>();
            try
            {
                contractId = OnlineRegAdmin.GetDataContractByRegId(regID).ToList();
            }
            catch (Exception ex)
            {

                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return contractId;
        }

        //CR for auto selection for Discount/Promotion component implementation by chetan
        public List<string> GetKenanCodeForReassign()
        {
            List<string> value = new List<string>();
            try
            {
                value = OnlineRegAdmin.GetKenanCodeForReassign();
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                 
            }
            return value;
        }

        public DeviceTypesGetResp GetDeviceType()
        {
            DeviceTypesGetResp resp = new DeviceTypesGetResp();
            List<DeviceTypes> value = new List<DeviceTypes>();
            try
            {
                value = OnlineRegAdmin.GetDeviceType();
                resp.DeviceTypes = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return resp;
        }

        public DeviceCapacityGetResp GetDeviceCapacity()
        {
            DeviceCapacityGetResp resp = new DeviceCapacityGetResp();
            List<DeviceCapacity> value = new List<DeviceCapacity>();
            try
            {
                value = OnlineRegAdmin.GetDeviceCapacity();
                resp.DeviceCapacity = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return resp;
        }

        public PriceRangeGetResp GetPriceRange()
        {
            PriceRangeGetResp resp = new PriceRangeGetResp();
            List<PriceRange> value = new List<PriceRange>();
            try
            {
                value = OnlineRegAdmin.GetPriceRange();
                resp.PriceRange = value;
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return resp;
        }
 

        #region Devices for Smart SuppLines
        public List<BrandSmart> GetBrandSmartsForSuppLines()
        {
            List<BrandSmart> objBrandExt = null;
            try
            {
                objBrandExt = OnlineRegAdmin.GetBrandSmartsForSupplines();
                if (objBrandExt != null)
                {
                    return objBrandExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objBrandExt;

        }
        #endregion

        #region Models for smart

        public List<ModelSmart> GetSmartModelsForSupplines(int Brandid)
        {
            List<ModelSmart> objModelExt = null;
            try
            {
                objModelExt = OnlineRegAdmin.GetSmartDevicesForSupplines(Brandid);
                if (objModelExt != null)
                {
                    return objModelExt;

                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));

            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objModelExt;

        }

        #endregion


        #region contract Duration by kenan code

        public int GetContractDurationByKenanCode(string kenanCode)
        {

            int oRp = 0;
            try
            {
                oRp = OnlineRegAdmin.GetContractDurationByKenanCode(kenanCode);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }

        #endregion


        public List<lnkAcctMktCodePackages> GetPlansbyMarketandAccountCategory(int AcctCategory, int MarketCode)
        {
            List<lnkAcctMktCodePackages> value = new List<lnkAcctMktCodePackages>();
            try
            {
                value = OnlineRegAdmin.GetPlansbyMarketandAccountCategory(AcctCategory, MarketCode);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return value;
        }

		public List<lnkAcctMktCodeOffers> GetOffersbyMarketandAccountCategory(int AcctCategory, int MarketCode)
        {
			List<lnkAcctMktCodeOffers> value = new List<lnkAcctMktCodeOffers>();
            try
            {
				value = OnlineRegAdmin.GetOffersbyMarketandAccountCategory(AcctCategory, MarketCode);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return value;
        }
		

        public PackageComponentsResp GetLnkPgmBdlPkgComp(PgmBdlPckComponentFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PgmBdlPckComponentGet1", svcName);
            PackageComponentsResp oRp = new PackageComponentsResp();
            try
            {
                oRp.ComponentsZippedData = SNT.Utility.Compress.Zip(Util.ConvertObjectToXml(OnlineRegAdmin.ProgramBundlePackageComponentGet1(oReq.PgmBdlPckComponentFind)));
                //  string compressedResponse = SNT.Utility.Compress.Zip(Util.ConvertObjectToXml(results));
                // oRp.PgmBdlPckComponentsCompressed = compressedResponse;
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PgmBdlPckComponentGet", svcName);
            }
            return oRp;
        }

        public List<Region> GetRegion()
        {
            List<Region> result = null;
            try
            {
                result = OnlineRegAdmin.GetRegion();
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                result = null;
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.GetRegion({1})", svcName, "");
            }
            return result;
        }

        public List<Registration.DAL.Models.refLovList> GetLovList(string lovtype)
        {
            List<refLovList> objLovList = null;
            try
            {
                objLovList = OnlineRegAdmin.GetLovList(lovtype);
                if (objLovList != null)
                {
                    return objLovList;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                // Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return objLovList;
        }

		public List<Registration.DAL.Models.refLovList> GetAllLovList()
		{
			List<refLovList> objLovList = null;
			try
			{
				objLovList = OnlineRegAdmin.GetAllLovList();
				if (objLovList != null)
				{
					return objLovList;
				}
				else
				{
					return null;
				}

			}
			catch (Exception ex)
			{
				Logger.Error(" Error: " + Util.LogException(ex));
			}
			finally
			{
				// Logger.InfoFormat("Exiting {0}.PlanPropertyGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
			}
			return objLovList;
		}

		public List<DeviceFinancingInfo> GetAllDeviceFinancingInfo()
		{
			List<DeviceFinancingInfo> result = null;
			try
			{
				result = OnlineRegAdmin.GetAllDeviceFinancingInfo();
			}
			catch (Exception ex)
			{
				Logger.Error(" Error: " + Util.LogException(ex));
				result = null;
			}
			finally
			{
				Logger.InfoFormat("Entering {0}.GetAllDeviceFinancingInfo({1})", svcName, "");
			}
			return result;
		}

		public List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleIDs(List<string> _articleIDs)
		{
			List<DeviceFinancingInfo> result = null;
			try
			{
				result = OnlineRegAdmin.GetDeviceFinancingInfoByArticleIDs(_articleIDs);
			}
			catch (Exception ex)
			{
				Logger.Error(" Error: " + Util.LogException(ex));
				result = null;
			}
			finally
			{
				Logger.InfoFormat("Entering {0}.GetDeviceFinancingInfoByArticleIDs({1})", svcName, "");
			}
			return result;
		}

		public List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleID(string _articleID, int planID)
		{
			List<DeviceFinancingInfo> result = null;
			try
			{
				result = OnlineRegAdmin.GetDeviceFinancingInfoByArticleID(_articleID, planID);
			}
			catch (Exception ex)
			{
				Logger.Error(" Error: " + Util.LogException(ex));
				result = null;
			}
			finally
			{
				Logger.InfoFormat("Entering {0}.GetDeviceFinancingInfoByArticleID({1})", svcName, "");
			}
			return result;
		}

        public List<Accessory> GetAccessoryDetailsByEANKey(string eanKey)
        {
            var accessory = new List<Accessory>();
            try
            {
                accessory = OnlineRegAdmin.GetAccessoryDetailsByEANKey(eanKey);
                Logger.Debug("GetAccessoryDetailsByEANKey, parameter: " + eanKey);
                Logger.Debug(accessory);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetAccessoryDetailsByEANKey, parameter: " + eanKey);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return accessory;
        }

        public List<Accessory> GetAccessoryDetailsByArticleID(string articleID)
        {
            var accessory = new List<Accessory>();
            try
            {
                accessory = OnlineRegAdmin.GetAccessoryDetailsByArticleID(articleID);
                Logger.Debug("GetAccessoryDetailsByArticleID, parameter: " + articleID);
                Logger.Debug(accessory);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetAccessoryDetailsByArticleID, parameter: " + articleID);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return accessory;
        }

        public List<Accessory> GetAccessoryDetailsByName(string _accessoryName)
        {
            var accessory = new List<Accessory>();
            var maxSearchResult = Properties.Settings.Default.maxSearchResultForAccessory;

            try
            {
                accessory = OnlineRegAdmin.GetAccessoryDetailsByName(_accessoryName, maxSearchResult);
                Logger.Debug("GetAccessoryDetailsByName, parameter: " + _accessoryName);
                Logger.Debug(accessory);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetAccessoryDetailsByName, parameter: " + _accessoryName);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return accessory;
        }

        public string GetEANKeyByArticleID(string articleID)
        {
            var eanKey = string.Empty;
            try
            {
                eanKey = OnlineRegAdmin.GetEANKeyByArticleID(articleID);
                Logger.Debug("GetEANKeyByArticleID, parameter: " + articleID);
                Logger.Debug(eanKey);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetEANKeyByArticleID, parameter: " + articleID);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return eanKey;
        }

        public List<OfferAccessory> GetOfferAccessoryDetails(List<string> articleIDs)
        {
            var offerAccessory = new List<OfferAccessory>();
            try
            {
                offerAccessory = OnlineRegAdmin.GetOfferAccessoryDetails(articleIDs);
                Logger.Debug("GetAccessoryDetailsByArticleID, parameter: " + articleIDs);
                Logger.Debug(offerAccessory);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetOfferAccessoryDetails, parameter: " + articleIDs);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return offerAccessory;
        }

        public List<OfferAccessory> GetAccessoryAvailability(List<OfferAccessory> offerAccessories, int orgID)
        {
            var offerAccessory = new List<OfferAccessory>();
            try
            {
                offerAccessory = OnlineRegAdmin.GetAccessoryAvailability(offerAccessories, orgID);
                Logger.Debug("GetAccessoryAvailability, parameter 1: " + offerAccessories);
                Logger.Debug("GetAccessoryAvailability, parameter 2: " + orgID);
                Logger.Debug(offerAccessory);
            }
            catch (Exception ex)
            {
                Logger.Error("Error in GetOfferAccessoryDetails, parameter 1: " + offerAccessories);
                Logger.Error("Error in GetOfferAccessoryDetails, parameter 2: " + orgID);
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            return offerAccessory;
        }
		
		public List<AccFinancingInfo> GetAllAccessoriesFinancingInfo()
		{
			var accFinancingInfoList = new List<AccFinancingInfo>();
			try
			{
				accFinancingInfoList = OnlineRegAdmin.GetAllAccessoriesFinancingInfo();
			}
			catch (Exception ex)
			{
				Logger.Error(string.Format("accFinancingInfoList - ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
				accFinancingInfoList = null;
			}
			return accFinancingInfoList;
		}
    }
}
