﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;
using SNT.Utility;
using Online.Registration.Service.Models;
using Online.Registration.DAL.Admin;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OrganizationService" in code, svc and config file together.
    public class OrganizationService : IOrganizationService
    {
        static string svcName = "OrganizationService";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);
        public OrganizationService()
        {
            //if (log4net.LogManager.GetCurrentLoggers().Count() == 0)
            //{
            //    log4net.Config.XmlConfigurator.Configure();
            //}
            Logger.InfoFormat("{0} initialized....", svcName);
        }

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.StackTrace = ex.StackTrace;
        }

        #endregion

        #region Org Type

        public OrgTypeCreateResp OrgTypeCreate(OrgTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeCreate({1})", svcName, (oReq != null) ? oReq.OrgType.Name : "NULL");

            var resp = new OrgTypeCreateResp();

            try 
            { 
                resp.ID = OnlineRegAdmin.OrgTypeCreate(oReq.OrgType); 
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message; //+ "|" + 
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally 
            { 
                Logger.InfoFormat("Exiting {0}.OrgTypeCreate({1}): {2}|{3}", svcName, (oReq != null) ? oReq.OrgType.Name : "NULL", resp.ID, resp.Code); 
            }

            return resp;
        }
        public OrgTypeUpdateResp OrgTypeUpdate(OrgTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeUpdate({1})", svcName, (oReq != null) ? oReq.OrgType.ID : 0);
            var resp = new OrgTypeUpdateResp();

            try
            {
                int count = OnlineRegAdmin.OrgTypeUpdate(oReq.OrgType);
                Logger.DebugFormat("{0}.TCMRegAdmin.OrgTypeUpdate({1}):{2}", svcName, (oReq != null) ? oReq.OrgType.ID : 0, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;  // +"|" + 
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally 
            { 
                Logger.InfoFormat("Exiting {0}.OrgTypeUpdate({1}): {2}", svcName, (oReq != null) ? oReq.OrgType.ID : 0, resp.Code); 
            }

            return resp;
        }
        public OrgTypeFindResp OrgTypeFind(OrgTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeFind({1})", svcName, (oReq != null) ? oReq.OrgTypeFind.OrgType.Name : "NULL");
            var resp = new OrgTypeFindResp();

            try { resp.IDs = OnlineRegAdmin.OrgTypeFind(oReq.OrgTypeFind); }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;  // +"|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally 
            {
                Logger.InfoFormat("Exiting {0}.OrgTypeFind({1}): {2}", svcName, (oReq != null) ? oReq.OrgTypeFind.OrgType.Name : "NULL", resp.Code); 
            }

            return resp;
        }
        public OrgTypeGetResp OrgTypeGet(OrgTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrgTypeGet({1})", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0);
            var resp = new OrgTypeGetResp();

            try
            {
                if (oReq == null)
                {
                    resp.Code = "-1";
                    resp.Message = "Error: Request object cannot be NULL/Empty!";
                }
                else { resp.OrgTypes = OnlineRegAdmin.OrgTypeGet(oReq.IDs); }
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.OrgTypeGet({1}): {2}", svcName, (oReq != null && oReq.IDs.Count > 0) ? oReq.IDs[0] : 0, resp.Code); 
            }

            return resp;
        }

        public OrgTypeGetResp OrgTypesList()
        {
            Logger.InfoFormat("Entering {0}.OrgTypesList({1})", svcName, "");
            var resp = new OrgTypeGetResp();
            try
            {
                resp.OrgTypes = OnlineRegAdmin.OrgTypesList(); 
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message; //+ "|" + 
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.OrgTypeGet({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        #region Organization

        public OrganizationCreateResp OrganizationCreate(OrganizationCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationCreate({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            OrganizationCreateResp resp = new OrganizationCreateResp();
            try
            {
                resp.ID = OnlineRegAdmin.OrganizationCreate(oReq.Organization);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                resp.ID = 0;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationCreate({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            }
            return resp;
        }
        public OrganizationUpdateResp OrganizationUpdate(OrganizationUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationUpdate({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            OrganizationUpdateResp resp = new OrganizationUpdateResp();
            int count = 0;
            try
            {
                count = OnlineRegAdmin.OrganizationUpdate(oReq.Organization);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationUpdate({1})", svcName, (oReq != null) ? oReq.Organization.Name : "NULL");
            }
            return resp;
        }
        public OrganizationGetResp OrganizationGet(OrganizationGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationGet({1})", svcName, (oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0].ToString() : "NULL");
            OrganizationGetResp resp = new OrganizationGetResp();
            try
            {
                resp.Organizations = OnlineRegAdmin.OrganizationGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationGet({1})", svcName, (oReq != null && oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0].ToString() : "NULL");
            }
            return resp;
        }

        public OrganizationGetResp OrganizationList()
        {
            Logger.InfoFormat("Entering {0}.OrganizationList({1})", svcName, "");
            OrganizationGetResp resp = new OrganizationGetResp();
            try
            {
                resp.Organizations = OnlineRegAdmin.OrganizationList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationList({1})", svcName, "");
            }
            return resp;
        }
        public OrganizationFindResp OrganizationFind(OrganizationFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.OrganizationFind({1})", svcName, (oReq != null) ? oReq.OrganizationFind.Organization.Name : "NULL");
            OrganizationFindResp resp = new OrganizationFindResp();
            try
            {
                //resp.Organizations = TCMRegAdmin.OrganizationGet(oReq.);
                resp.IDs = OnlineRegAdmin.OrganizationFind(oReq.OrganizationFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationFind({1})", svcName, (oReq != null) ? oReq.OrganizationFind.Organization.Name : "NULL");
            }
            return resp;
        }

        public OrganizationGetResp OrganizationGetAll()
        {
            Logger.InfoFormat("Entering {0}.OrganizationGetALL({1})", svcName);
            OrganizationGetResp resp = new OrganizationGetResp();
            try
            {
                resp.Organizations = OnlineRegAdmin.OrganizationGetALL();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Entering {0}.OrganizationGetALL({1})", svcName);
            }
            return resp;
        }

        #endregion
    }
}
