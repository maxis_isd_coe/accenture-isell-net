﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.DirectoryServices.AccountManagement;

using log4net;
using SNT.Utility;
using Online.Registration.Service.Models;
using Online.Registration.DAL.Admin;
using Online.Registration.DAL.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in code, svc and config file together.
    public class UserService : IUserService
    {
        static string svcName = "UserService";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);
        public UserService()
        {
            //if (log4net.LogManager.GetCurrentLoggers().Count() == 0)
            //{
            //    log4net.Config.XmlConfigurator.Configure();
            //}
            Logger.InfoFormat("{0} initialized....", svcName);
        }

        #region User Group Access

        public UserGrpAccessCreateResp UserGrpAccessCreate(UserGrpAccessCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessCreate({1})", svcName, oReq.UserGrpAccess.ID);

            UserGrpAccessCreateResp resp = new UserGrpAccessCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserGrpAccessCreate(oReq.UserGrpAccess);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpAccessCreate({1}): {2}|{3}", svcName, oReq.UserGrpAccess.ID, resp.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpAccessUpdateResp UserGrpAccessUpdate(UserGrpAccessUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessUpdate({1})", svcName, oReq.UserGrpAccess.ID);
            UserGrpAccessUpdateResp resp = new UserGrpAccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.UserGrpAccessUpdate(oReq.UserGrpAccess);
                Logger.DebugFormat("{0}.TCMRegAdmin.UserGrpAccessUpdate({1}):{2}", svcName, oReq.UserGrpAccess.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpAccessUpdate({1}): {2}", svcName, oReq.UserGrpAccess.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpAccessFindResp UserGrpAccessFind(UserGrpAccessFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessFind({1})", svcName, oReq.UserGrpAccessFind.UserGrpAccess.ID);
            UserGrpAccessFindResp resp = new UserGrpAccessFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserGrpAccessFind(oReq.UserGrpAccessFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpAccessFind({1}): {2}", svcName, oReq.UserGrpAccessFind.UserGrpAccess.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpAccessGetResp UserGrpAccessGet(UserGrpAccessGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpAccessGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count() : 0);
            UserGrpAccessGetResp resp = new UserGrpAccessGetResp();

            try
            {
                resp.UserGrpAccesss = OnlineRegAdmin.UserGrpAccessGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpAccessFind({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count() : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region User Group Role

        public UserGrpRoleCreateResp UserGrpRoleCreate(UserGrpRoleCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleCreate({1})", svcName, oReq.UserGrpRole.ID);

            UserGrpRoleCreateResp resp = new UserGrpRoleCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserGrpRoleCreate(oReq.UserGrpRole);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpRoleCreate({1}): {2}|{3}", svcName, oReq.UserGrpRole.ID, resp.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpRoleUpdateResp UserGrpRoleUpdate(UserGrpRoleUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleUpdate({1})", svcName, oReq.UserGrpRole.ID);
            UserGrpRoleUpdateResp resp = new UserGrpRoleUpdateResp();

            try
            {
                int count = OnlineRegAdmin.UserGrpRoleUpdate(oReq.UserGrpRole);
                Logger.DebugFormat("{0}.TCMRegAdmin.UserGrpRoleUpdate({1}):{2}", svcName, oReq.UserGrpRole.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpRoleUpdate({1}): {2}", svcName, oReq.UserGrpRole.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpRoleFindResp UserGrpRoleFind(UserGrpRoleFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleFind({1})", svcName, oReq.UserGrpRoleFind.UserGrpRole.ID);
            UserGrpRoleFindResp resp = new UserGrpRoleFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserGrpRoleFind(oReq.UserGrpRoleFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpRoleFind({1}): {2}", svcName, oReq.UserGrpRoleFind.UserGrpRole.ID, resp.Code);
            }

            return resp;
        }
        public UserGrpRoleGetResp UserGrpRoleGet(UserGrpRoleGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGrpRoleGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count : 0);
            UserGrpRoleGetResp resp = new UserGrpRoleGetResp();

            try
            {
                resp.UserGrpRoles = OnlineRegAdmin.UserGrpRoleGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGrpRoleFind({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs.Count : 0, resp.Code);
            }

            return resp;
        }

        #endregion

        #region User Group

        public UserGroupCreateResp UserGroupCreate(UserGroupCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupCreate({1})", svcName, oReq.UserGroup.Name);

            UserGroupCreateResp resp = new UserGroupCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserGroupCreate(oReq.UserGroup);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGroupCreate({1}): {2}|{3}", svcName, oReq.UserGroup.Name, resp.ID, resp.Code);
            }

            return resp;
        }
        public UserGroupUpdateResp UserGroupUpdate(UserGroupUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupUpdate({1})", svcName, oReq.UserGroup.Name);
            UserGroupUpdateResp resp = new UserGroupUpdateResp();

            try
            {
                int count = OnlineRegAdmin.UserGroupUpdate(oReq.UserGroup);
                Logger.DebugFormat("{0}.TCMRegAdmin.UserGroupUpdate({1}):{2}", svcName, oReq.UserGroup.Name, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGroupUpdate({1}): {2}", svcName, oReq.UserGroup.Name, resp.Code);
            }

            return resp;
        }
        public UserGroupFindResp UserGroupFind(UserGroupFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupFind({1})", svcName, oReq.UserGroupFind.UserGroup.Name);
            UserGroupFindResp resp = new UserGroupFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserGroupFind(oReq.UserGroupFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGroupFind({1}): {2}", svcName, oReq.UserGroupFind.UserGroup.Name, resp.Code);
            }

            return resp;
        }
        public UserGroupGetResp UserGroupGet(UserGroupGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGroupGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0);
            UserGroupGetResp resp = new UserGroupGetResp();

            try
            {
                resp.UserGroups = OnlineRegAdmin.UserGroupGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGroupFind({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        public UserGroupGetResp UserGroupList()
        {
            Logger.InfoFormat("Entering {0}.UserGroupList({1})", svcName, "");
            UserGroupGetResp resp = new UserGroupGetResp();

            try
            {
                resp.UserGroups = OnlineRegAdmin.UserGroupList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserGroupList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        #region User Role

        public UserRoleCreateResp UserRoleCreate(UserRoleCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleCreate({1})", svcName, oReq.UserRole.Code);

            UserRoleCreateResp resp = new UserRoleCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserRoleCreate(oReq.UserRole);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserRoleCreate({1}): {2}|{3}", svcName, oReq.UserRole.Code, resp.ID, resp.Code);
            }

            return resp;
        }
        public UserRoleUpdateResp UserRoleUpdate(UserRoleUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleUpdate({1})", svcName, oReq.UserRole.Code);
            UserRoleUpdateResp resp = new UserRoleUpdateResp();

            try
            {
                int count = OnlineRegAdmin.UserRoleUpdate(oReq.UserRole);
                Logger.DebugFormat("{0}.TCMRegAdmin.UserRoleUpdate({1}):{2}", svcName, oReq.UserRole.Code, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserRoleUpdate({1}): {2}", svcName, oReq.UserRole.Code, resp.Code);
            }

            return resp;
        }
        public UserRoleFindResp UserRoleFind(UserRoleFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleFind({1})", svcName, oReq.UserRoleFind.UserRole.Code);
            UserRoleFindResp resp = new UserRoleFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserRoleFind(oReq.UserRoleFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserRoleFind({1}): {2}", svcName, oReq.UserRoleFind.UserRole.Code, resp.Code);
            }

            return resp;
        }
        public UserRoleGetResp UserRoleGet(UserRoleGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserRoleGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0);
            UserRoleGetResp resp = new UserRoleGetResp();

            try
            {
                resp.UserRoles = OnlineRegAdmin.UserRoleGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserRoleFind({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }
        public UserRoleGetResp UserRolesList()
        {
            Logger.InfoFormat("Entering {0}.UserRolesList({1})", svcName,"");
            UserRoleGetResp resp = new UserRoleGetResp();

            try
            {
                resp.UserRoles = OnlineRegAdmin.UserRolesList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserRolesList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }





        #endregion

        #region Access

        public AccessCreateResp AccessCreate(AccessCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessCreate({1})", svcName, oReq.Access.UserName);

            AccessCreateResp resp = new AccessCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.AccessCreate(oReq.Access);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessCreate({1}): {2}|{3}", svcName, oReq.Access.UserName, resp.ID, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessUpdate(AccessUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessUpdate({1})", svcName, oReq.Access.UserName);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessUpdate(oReq.Access);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessUpdate({1}):{2}", svcName, oReq.Access.UserName, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessUpdate({1}): {2}", svcName, oReq.Access.UserName, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessPasswordUpdate(AccessUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessPasswordUpdate({1})", svcName, oReq.Access.ID);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessPasswordUpdate(oReq.Access);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessPasswordUpdate({1}):{2}", svcName, oReq.Access.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessPasswordUpdate({1}): {2}", svcName, oReq.Access.ID, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessFailedPasswordAttemptAdd(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessFailedPasswordAttemptAdd({1})", svcName, accessID);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessFailedPasswordAttemptAdd(accessID);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessFailedPasswordAttemptAdd({1}):{2}", svcName, accessID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessFailedPasswordAttemptAdd({1}): {2}", svcName, accessID, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessFailedPasswordAttemptReset(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessFailedPasswordAttemptReset({1})", svcName, accessID);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessFailedPasswordAttemptReset(accessID);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessFailedPasswordAttemptReset({1}):{2}", svcName, accessID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessFailedPasswordAttemptReset({1}): {2}", svcName, accessID, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessLockedOut(int accessID)
        {
            Logger.InfoFormat("Entering {0}.AccessLockedOut({1})", svcName, accessID);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessLockedOut(accessID);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessLockedOut({1}):{2}", svcName, accessID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessLockedOut({1}): {2}", svcName, accessID, resp.Code);
            }

            return resp;
        }
        public AccessUpdateResp AccessUnlock(int accessID, string lastAccessID)
        {
            Logger.InfoFormat("Entering {0}.AccessUnlock({1})", svcName, accessID);
            AccessUpdateResp resp = new AccessUpdateResp();

            try
            {
                int count = OnlineRegAdmin.AccessUnlock(accessID, lastAccessID);
                Logger.DebugFormat("{0}.TCMRegAdmin.AccessUnlock({1}):{2}", svcName, accessID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessUnlock({1}): {2}", svcName, accessID, resp.Code);
            }

            return resp;
        }
        public AccessFindResp AccessFind(AccessFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessFind({1})", svcName, oReq.AccessFind.Access.UserName);
            AccessFindResp resp = new AccessFindResp();

            try
            {
                //PrincipalContext domain = new PrincipalContext(ContextType.ApplicationDirectory, "sgbsld01.isddc.men.maxis.com.my", "o=users", "cn=LDAP_IPC,ou=netservice,o=users", "Password123");

                //UserPrincipal user = UserPrincipal.FindByIdentity(domain, "ADAM"); 

                resp.IDs = OnlineRegAdmin.AccessFind(oReq.AccessFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessFind({1}): {2}", svcName, oReq.AccessFind.Access.UserName, resp.Code);
            }

            return resp;
        }
        public AccessGetResp AccessGet(AccessGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.AccessGet({1})", svcName, (oReq.IDs != null) ? oReq.IDs.Count : 0);
            AccessGetResp resp = new AccessGetResp();

            try
            {
                resp.Accesss = OnlineRegAdmin.AccessGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessFind({1}): {2}", svcName, (oReq.IDs != null) ? oReq.IDs.Count : 0, resp.Code);
            }

            return resp;
        }

        public AccessGetResp AccessList()
        {
            Logger.InfoFormat("Entering {0}.AccessList({1})", svcName, "");
            AccessGetResp resp = new AccessGetResp();

            try
            {
                resp.Accesss = OnlineRegAdmin.AccessList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.AccessList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        #region User

        public UserCreateResp UserCreate(UserCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserCreate({1})", svcName, oReq.User.FullName);

            UserCreateResp resp = new UserCreateResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserCreate(oReq.User);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserCreate({1}): {2}|{3}", svcName, oReq.User.FullName, resp.ID, resp.Code);
            }

            return resp;
        }
        public UserUpdateResp UserUpdate(UserUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserUpdate({1})", svcName, oReq.User.ID);
            UserUpdateResp resp = new UserUpdateResp();

            try
            {
                int count = OnlineRegAdmin.UserUpdate(oReq.User);
                Logger.DebugFormat("{0}.TCMRegAdmin.UserUpdate({1}):{2}", svcName, oReq.User.ID, count);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                //Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserUpdate({1}): {2}", svcName, oReq.User.ID, resp.Code);
            }

            return resp;
        }
        public UserFindResp UserFind(UserFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserFind({1})", svcName, oReq.UserFind.User.FullName);
            UserFindResp resp = new UserFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserFind(oReq.UserFind);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserFind({1}): {2}", svcName, oReq.UserFind.User.FullName, resp.Code);
            }

            return resp;
        }
        public UserFindResp UserFindbyName(UserFindbyNameReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserFind({1})", svcName, oReq.UserFindbyName);
            UserFindResp resp = new UserFindResp();

            try
            {
                resp.IDs = OnlineRegAdmin.UserFindbyName(oReq.UserFindbyName);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserFind({1}): {2}", svcName, oReq.UserFindbyName, resp.Code);
            }

            return resp;
        }
        public UserGetResp UserGet(UserGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserGet({1})", svcName, (oReq.IDs != null && oReq.IDs.Count != 0) ? oReq.IDs[0] : 0);
            UserGetResp resp = new UserGetResp();

            try
            {
                resp.Users = OnlineRegAdmin.UserGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserFind({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count != 0) ? oReq.IDs[0] : 0, resp.Code);
            }

            return resp;
        }

        public UserGetResp UsersList()
        {
            Logger.InfoFormat("Entering {0}.UsersList({1})", svcName, "");
            UserGetResp resp = new UserGetResp();
            try
            {
                resp.Users = OnlineRegAdmin.UsersList();
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UsersList({1}): {2}", svcName, "", resp.Code);
            }

            return resp;
        }

        #endregion

        public UsersInRoleFindResp UsersInRoleFind(UsersInRoleFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UsersInRoleFind({1}, {2})", svcName, oReq.RoleCode, oReq.UserName);
            UsersInRoleFindResp resp = new UsersInRoleFindResp();

            try
            {
                resp.Users = OnlineRegAdmin.UsersInRoleFind(oReq.RoleCode, oReq.UserName, oReq.Active);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UsersInRoleFind({1}): {2}", svcName, oReq.RoleCode, resp.Code);
            }

            return resp;
        }

        public RolesForUserGetResp RolesForUserGet(RolesForUserGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.RolesForUserGet({1})", svcName, oReq.UserName);
            RolesForUserGetResp resp = new RolesForUserGetResp();

            try
            {
                resp.Roles = OnlineRegAdmin.RolesForUserGet(oReq.UserName, oReq.Active);
            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.RolesForUserGet({1}): {2}", svcName, oReq.UserName, resp.Code);
            }

            return resp;
        }

        
        #region VLT ADDED CODE
        public GetAPIStatusMessagesResp GetAPIStatusMessages(GetAPIStatusMessagesReq oReq)
        {
            Logger.InfoFormat("Entering {0}.GetAPIStatusMessagesCreate({1})", svcName, oReq.tblAPIStatusMessagesval.APIName);

            GetAPIStatusMessagesResp resp = new GetAPIStatusMessagesResp();

            try
            {
                resp.tblAPIStatusMessageslst = OnlineRegAdmin.APIStatusMessagesGet(oReq.tblAPIStatusMessagesval);
            }
            catch (Exception ex)
            {
                resp.tblAPIStatusMessageslst = null;
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetAPIStatusMessages({1}): {2}|{3}", svcName, oReq.tblAPIStatusMessagesval.APIName, (resp.tblAPIStatusMessageslst != null) ? Convert.ToString(resp.tblAPIStatusMessageslst[0].ID) : null, resp.Code);
            }

            return resp;
        }
        #endregion VLT ADDED CODE

        public GetDonerMessagesResp GetDonors()
        {
            Logger.InfoFormat("Entering {0}.", svcName);

            GetDonerMessagesResp resp = new GetDonerMessagesResp();

            try
            {
                resp.tblDonorlst = OnlineRegAdmin.GetDonors();
            }
            catch (Exception ex)
            {
                resp.tblDonorlst = null;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetDonors({1}): {2}", svcName, "GetDonors", resp.Code);
            }

            return resp;
        }

        #region VLT ADDED CODE
        /// <summary>
        /// APIs the save call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public APISaveCallStatusResp APISaveCallStatus(APISaveCallStatusReq oReq)
        {
            Logger.InfoFormat("Entering {0}.APISaveCallStatus({1})", svcName, oReq.tblAPICallStatusMessagesval.APIName);

            APISaveCallStatusResp resp = new APISaveCallStatusResp();

            try
            {
                resp.APIID = OnlineRegAdmin.APISaveCallStatus(oReq.tblAPICallStatusMessagesval);
            }
            catch (Exception ex)
            {
                resp.APIID = null;
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.APISaveCallStatus({1}): {2}|{3}", svcName, oReq.tblAPICallStatusMessagesval.APIName, (resp.APIID != null) ? Convert.ToString(resp.APIID) : null, resp.Code);
            }

            return resp;
        }

        /// <summary>
        /// APIs the update call status.
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public APISaveCallStatusResp APIUpdateCallStatus(APISaveCallStatusReq oReq)
        {
            Logger.InfoFormat("Entering {0}.APIUpdateCallStatus({1})", svcName, oReq.tblAPICallStatusMessagesval.APIName);

            APISaveCallStatusResp resp = new APISaveCallStatusResp();

            try
            {
                resp.APIID = OnlineRegAdmin.APIUpdateCallStatus(oReq.tblAPICallStatusMessagesval);
            }
            catch (Exception ex)
            {
                resp.APIID = null;
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.APIUpdateCallStatus({1}): {2}|{3}", svcName, oReq.tblAPICallStatusMessagesval.APIName, (resp.APIID != null) ? Convert.ToString(resp.APIID) : null, resp.Code);
            }

            return resp;
        }

        /// <summary>
        /// Get min values for the item
        /// </summary>
        /// <param name="oReq"></param>
        /// <returns></returns>
        public PlanSearchItemResp PlanSearchItemMinRange(PlanSearchItemReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PlanSearchItemMinRange({1})", svcName, oReq.planSearchVal);

            PlanSearchItemResp resp = new PlanSearchItemResp();

            try
            {
                resp.VoiceUsage = OnlineRegAdmin.PlanPropertyValuesGetMin(oReq.planSearchVal);
            }
            catch (Exception ex)
            {
                resp.VoiceUsage = null;
                //resp.Message = ex.Message;
                //resp.StackTrace = ex.StackTrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg, out stacktrace, false);
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PlanSearchItemRange({1}): {2}|{3}", svcName, oReq.planSearchVal, (resp.VoiceUsage != null) ? Convert.ToString(resp.VoiceUsage) : null, resp.Code);
            }

            return resp;

        }
        /// <summary>
        /// Gets the max values for the plan search items
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public PlanSearchItemResp PlanSearchItemMaxRange(PlanSearchItemReq oReq)
        {
            Logger.InfoFormat("Entering {0}.PlanSearchItemMaxRange({1})", svcName, oReq.planSearchVal);

            PlanSearchItemResp resp = new PlanSearchItemResp();

            try
            {
                resp.VoiceUsage = OnlineRegAdmin.PlanPropertyValuesGetMax(oReq.planSearchVal);
            }
            catch (Exception ex)
            {
                resp.VoiceUsage = null;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";

                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PlanSearchItemRange({1}): {2}|{3}", svcName, oReq.planSearchVal, (resp.VoiceUsage != null) ? Convert.ToString(resp.VoiceUsage) : null, resp.Code);
            }

            return resp;

        }

        #endregion VLT ADDED CODE

        #region VLT ADDED CODE by Rajeswari on Feb 25th for Logging user information

        /// <summary>
        /// inserts the user infomation
        /// </summary>
        /// <param name="oReq">The o req.</param>
        /// <returns></returns>
        public UserTransactionLogResp UserLogInfo(UserTransactionLogReq oReq)
        {
            Logger.InfoFormat("Entering {0}.UserTransactionLog({1})", svcName, oReq.UserInfoVal);

            UserTransactionLogResp resp = new UserTransactionLogResp();

            try
            {
                resp.ID = OnlineRegAdmin.UserInfoLogCreate(oReq.UserInfoVal);

            }
            catch (Exception ex)
            {
                resp.Code = "-1";
                string errmsg = "", stacktrace = "";
                resp.Message = errmsg;
                resp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.UserCreate({1}): {2}|{3}", svcName, oReq.UserInfoVal.ToString(), resp.ID, resp.Code);
            }

            return resp;

        }
        #endregion

        public bool MocICcheckwithwhitelistcustomers(string userName, string strIcno)
        {
            Logger.InfoFormat("Entering {0}.PlanSearchItemMaxRange({1})", svcName, userName);

            bool status = false;

            try
            {
                status = OnlineRegAdmin.MocICcheckwithwhitelistcustomers(userName, strIcno);
            }
            catch (Exception ex)
            {

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";

                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.PlanSearchItemRange({1}):", svcName, status);
            }

            return status;

        }

        public int GetPPIDCountSearch(string userName,string action)
        {
            Logger.InfoFormat("Entering {0}.GetPPIDCountSearch({1})", svcName, userName);
            int PPIDCount = 0;
            try
            {
                PPIDCount = OnlineRegAdmin.GetPPIDCountSearch(userName, action);
            }
            catch (Exception ex)
            {
             Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetPPIDCountSearch({1}):", svcName, PPIDCount);
            }

            return PPIDCount;
                
        }
        public List<UserConfigurations> GetUserConfigurations()
        {
            Logger.InfoFormat("Entering {0}.GetUserConfigurations({1})", svcName, "");
            List<UserConfigurations> objUserConfiguration = new List<UserConfigurations>();
            try
            {
                objUserConfiguration = OnlineRegAdmin.GetUserConfigurations().ToList();
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetUserConfigurations({1}):", svcName, "");
            }

            return objUserConfiguration;

        }
        public int SaveUserAuditLog(string userName, string sessionId, string ipAddress, int loggedOut)
        {
            Logger.InfoFormat("Entering {0}.SaveUserAuditLog({1})", svcName, userName);
            int logId = 0;
            try
            {
                logId = OnlineRegAdmin.SaveUserAuditLog(userName, sessionId, ipAddress, loggedOut);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveUserAuditLog({1}):", svcName, logId);
            }

            return logId;
        }
        public int SaveUserTransactionLog(int usrAuditLogId, int moduleId, string actionDone, string accountNo, string dealerNo)
        {
            Logger.InfoFormat("Entering {0}.SaveUserTransactionLog({1})", svcName, usrAuditLogId);
            int transactionId = 0;
            try
            {
                transactionId = OnlineRegAdmin.SaveUserTransactionLog(usrAuditLogId, moduleId, actionDone, accountNo, dealerNo);
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.SaveUserTransactionLog({1}):", svcName, transactionId);
            }

            return transactionId;
        }
    }
}
