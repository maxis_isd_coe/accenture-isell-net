﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CreditCardBatchFile" in code, svc and config file together.
    public class CreditCardBatchFile : ICreditCardBatchFile
    {
        public string GenerateFlatFile()
        {
            string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + Guid.NewGuid().ToString() + ".txt";

            // Create a stringbuilder and write the new user input to it.
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("New User Input");
            sb.AppendLine("= = = = = =");
            sb.Append("weeee");
            sb.AppendLine();
            sb.AppendLine();

            using (StreamWriter outfile = new StreamWriter(mydocpath, true))
            {
                outfile.Write(sb.ToString());
            }

            return mydocpath;
        }
    }
}
