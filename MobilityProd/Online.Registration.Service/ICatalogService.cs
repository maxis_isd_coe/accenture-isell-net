﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Online.Registration.DAL.Models;

using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICatalogService" in both code and config file together.
    [ServiceContract]
    public interface ICatalogService 
    {
        #region Program

        [OperationContract]
        ProgramCreateResp ProgramCreate(ProgramCreateReq oReq);
        [OperationContract]
        ProgramUpdateResp ProgramUpdate(ProgramUpdateReq oReq);
        [OperationContract]
        ProgramFindResp ProgramFind(ProgramFindReq oReq);
        [OperationContract]
        ProgramGetResp ProgramGet(ProgramGetReq oReq);
        [OperationContract]
        ProgramGetResp ProgramsList();

        #endregion

        #region Brand
        [OperationContract]
        BrandCreateResp BrandCreate(BrandCreateReq oReq);
        [OperationContract]
        BrandUpdateResp BrandUpdate(BrandUpdateReq oReq);
        [OperationContract]
        BrandGetResp BrandGet(BrandGetReq oReq);
        [OperationContract]
        BrandGetResp BrandsList();
        [OperationContract]
        BrandFindResp BrandFind(BrandFindReq oReq);
        #endregion

        #region Category
        [OperationContract]
        DataPlanIdsResp getFilterplanvalues(int plantypeid);
        [OperationContract]
        CategoryCreateResp CategoryCreate(CategoryCreateReq oReq);
        [OperationContract]
        CategoryUpdateResp CategoryUpdate(CategoryUpdateReq oReq);
        [OperationContract]
        CategoryGetResp CategoryGet(CategoryGetReq oReq);
        [OperationContract]
        CategoryGetResp CategoriesList();
        [OperationContract]
        CategoryFindResp CategoryFind(CategoryFindReq oReq);
        [OperationContract]
        Int32 GettllnkContractDataPlan(int selectedid);
        [OperationContract]
        List<int> GettllnkContractDataPlanList(List<int> selectedids);
        [OperationContract]
        List<Int32> GetDependentComponents(int componentId);
        [OperationContract]
        List<dependentContract> GetContractsOnOfferId(string offerIds);
        #endregion

        #region Bundle

        [OperationContract]
        BundleCreateResp BundleCreate(BundleCreateReq oReq);

        [OperationContract]
        BundleUpdateResp BundleUpdate(BundleUpdateReq oReq);

        [OperationContract]
        BundleFindResp BundleFind(BundleFindReq oReq);

        [OperationContract]
        BundleGetResp BundleGet(BundleGetReq oReq);
        [OperationContract]
        BundleGetResp BundlesList();

        #endregion

        #region Package
        [OperationContract]
        PackageCreateResp PackageCreate(PackageCreateReq oReq);
        [OperationContract]
        PackageUpdateResp PackageUpdate(PackageUpdateReq oReq);
        [OperationContract]
        PackageGetResp PackageGet(PackageGetReq oReq);
        [OperationContract]
        PackageGetResp PackagesList();
        [OperationContract]
        PackageFindResp PackageFind(PackageFindReq oReq);
        [OperationContract]
        BundlepackageResp GetMandatoryVas(int selectedid);
        [OperationContract]
        PackageComponentsResp GetLnkPgmBdlPkgComp(PgmBdlPckComponentFindReq oReq);
        #endregion

        #region Package Type
        [OperationContract]
        PackageTypeCreateResp PackageTypeCreate(PackageTypeCreateReq oReq);
        [OperationContract]
        PackageTypeUpdateResp PackageTypeUpdate(PackageTypeUpdateReq oReq);
        [OperationContract]
        PackageTypeGetResp PackageTypeGet(PackageTypeGetReq oReq);
        [OperationContract]
        PackageTypeGetResp PackageTypesList();
        [OperationContract]
        PackageTypeFindResp PackageTypeFind(PackageTypeFindReq oReq);
        #endregion

        #region Colour
        [OperationContract]
        ColourCreateResp ColourCreate(ColourCreateReq oReq);
        [OperationContract]
        ColourUpdateResp ColourUpdate(ColourUpdateReq oReq);
        [OperationContract]
        ColourGetResp ColourGet(ColourGetReq oReq);
        [OperationContract]
        ColourGetResp ColoursList();
        [OperationContract]
        ColourFindResp ColourFind(ColourFindReq oReq);

        #endregion

        #region Component
        [OperationContract]
        ComponentCreateResp ComponentCreate(ComponentCreateReq oReq);
        [OperationContract]
        ComponentUpdateResp ComponentUpdate(ComponentUpdateReq oReq);
        [OperationContract]
        ComponentGetResp ComponentGet(ComponentGetReq oReq);
        [OperationContract]
        ComponentFindResp ComponentFind(ComponentFindReq oReq);
        [OperationContract]
        ContractComponentGetResp ContractComponentGet();
        [OperationContract]
        List<int> GetDependentComponentList();
        [OperationContract]
        List<int> GetAllDependentComponents(int componentId, int planId);
        [OperationContract]
        List<int> GetAllSmartDependentComponents(int componentId, int planId);
        [OperationContract]
        List<int> GetAllDependentComponentsList(List<int> componentId, int planId);
        #endregion

        #region Component Type
        [OperationContract]
        ComponentTypeCreateResp ComponentTypeCreate(ComponentTypeCreateReq oReq);
        [OperationContract]
        ComponentTypeUpdateResp ComponentTypeUpdate(ComponentTypeUpdateReq oReq);
        [OperationContract]
        ComponentTypeGetResp ComponentTypeGet(ComponentTypeGetReq oReq);
        [OperationContract]
        ComponentTypeGetResp ComponentTypeList();
        [OperationContract]
        ComponentTypeFindResp ComponentTypeFind(ComponentTypeFindReq oReq);
        #endregion

        #region Component Relation
		[OperationContract]
		List<ComponentRelation> GetAllComponentRelationByRule(string rules);

        [OperationContract]
        List<ComponentRelation> ComponentRelationGet(int planBundle, string rules);

		[OperationContract]
		List<ComponentRelation> ComponentRelationGetByComponent(int componentPgm, string rules);
        #endregion

        #region Model
        [OperationContract]
        ModelCreateResp ModelCreate(ModelCreateReq oReq);
        [OperationContract]
        ModelUpdateResp ModelUpdate(ModelUpdateReq oReq);
        [OperationContract]
        ModelGetResp ModelGet(ModelGetReq oReq);
        [OperationContract]
        ModelGetResp ModelsList();
        [OperationContract]
        ModelFindResp ModelFind(ModelFindReq oReq);
        #endregion

        #region ModelImage
        [OperationContract]
        ModelImageCreateResp ModelImageCreate(ModelImageCreateReq oReq);
        [OperationContract]
        ModelImageUpdateResp ModelImageUpdate(ModelImageUpdateReq oReq);
        [OperationContract]
        ModelImageGetResp ModelImageGet(ModelImageGetReq oReq);
        [OperationContract]
        ModelImageFindResp ModelImageFind(ModelImageFindReq oReq);
        #endregion

        #region Model Group
        [OperationContract]
        ModelGroupCreateResp ModelGroupCreate(ModelGroupCreateReq oReq);
        [OperationContract]
        ModelGroupUpdateResp ModelGroupUpdate(ModelGroupUpdateReq oReq);
        [OperationContract]
        ModelGroupGetResp ModelGroupGet(ModelGroupGetReq oReq);
        [OperationContract]
        ModelGroupGetResp ModelGroupList();
        [OperationContract]
        ModelGroupGetResp ModelGroupGetByPBPCIDs(ModelGroupGetReq oReq);
        [OperationContract]
        ModelGroupFindResp ModelGroupFind(ModelGroupFindReq oReq);
        #endregion

        #region Program Bundle Package Component
        [OperationContract]
        PgmBdlPckComponentCreateResp PgmBdlPckComponentCreate(PgmBdlPckComponentCreateReq oReq);
        [OperationContract]
        PgmBdlPckComponentUpdateResp PgmBdlPckComponentUpdate(PgmBdlPckComponentUpdateReq oReq);
        [OperationContract]
        PgmBdlPckComponentGetResp PgmBdlPckComponentGet(PgmBdlPckComponentGetReq oReq);
        [OperationContract]
        PgmBdlPckComponentGetResp PgmBdlPckComponentGet1(PgmBdlPckComponentFindReq oReq);
        [OperationContract]
        PgmBdlPckComponentFindResp PgmBdlPckComponentFind(PgmBdlPckComponentFindReq oReq);
        [OperationContract]
        PkgCompContractGetResp PkgCompContractGet();
        [OperationContract]
        BdlPkgGetResp BdlPkgGet();
        [OperationContract]
        MISMMandatoryComponentGetResp MISMMandatoryComponentGet();
        #endregion

        #region Model Group Model
        [OperationContract]
        ModelGroupModelCreateResp ModelGroupModelCreate(ModelGroupModelCreateReq oReq);
        [OperationContract]
        ModelGroupModelUpdateResp ModelGroupModelUpdate(ModelGroupModelUpdateReq oReq);
        [OperationContract]
        ModelGroupModelGetResp ModelGroupModelGet(ModelGroupModelGetReq oReq);
        [OperationContract]
        ModelGroupModelFindResp ModelGroupModelFind(ModelGroupModelFindReq oReq);
        #endregion

        #region Model Part No

        [OperationContract]
        ModelPartNoCreateResp ModelPartNoCreate(ModelPartNoCreateReq oReq);
        [OperationContract]
        ModelPartNoUpdateResp ModelPartNoUpdate(ModelPartNoUpdateReq oReq);
        [OperationContract]
        ModelPartNoGetResp ModelPartNoGet(ModelPartNoGetReq oReq);
        [OperationContract]
        ModelPartNoFindResp ModelPartNoFind(ModelPartNoFindReq oReq);

        #endregion

        #region Item Price
        [OperationContract]
        ItemPriceCreateResp ItemPriceCreate(ItemPriceCreateReq oReq);
        [OperationContract]
        ItemPriceUpdateResp ItemPriceUpdate(ItemPriceUpdateReq oReq);
        [OperationContract]
        ItemPriceGetResp ItemPriceGet(ItemPriceGetReq oReq);
        [OperationContract]
        ItemPriceFindResp ItemPriceFind(ItemPriceFindReq oReq);
        [OperationContract]
        ItemPriceFindResp ItemPriceFindForMultipleContratsAndPlans(ItemPriceFindReq oReq);
        #endregion

        #region Advance & Deposit Price for Device
        //added by Deepika
        [OperationContract]
        AdvDepositPriceGetResp AdvDepositPriceGet(AdvDepositPriceGetReq oReq);
        //end Deepika

        [OperationContract]
        AdvDepositPriceGetResp AdvDepositPriceGetBySP(AdvDepositPriceGetReqStr oReq);
        #endregion

        #region Property

        [OperationContract]
        PropertyGetResp PropertyGet(PropertyGetReq oReq);
        [OperationContract]
        PropertyGetResp PropertyList();
        [OperationContract]
        PropertyFindResp PropertyFind(PropertyFindReq oReq);

        #endregion

        #region Property Value

        [OperationContract]
        PropertyValueGetResp PropertyValueGet(PropertyValueGetReq oReq);
        [OperationContract]
        PropertyValueFindResp PropertyValueFind(PropertyValueFindReq oReq);

        #endregion

        #region PlanProperty

        [OperationContract]
        PlanPropertyGetResp PlanPropertyGet(PlanPropertyGetReq oReq);
        [OperationContract]
        PlanPropertyFindResp PlanPropertyFind(PlanPropertyFindReq oReq);

        #endregion

        #region DeviceProperty

        [OperationContract]
        DevicePropertyGetResp DevicePropertyGet(DevicePropertyGetReq oReq);
        [OperationContract]
        DevicePropertyFindResp DevicePropertyFind(DevicePropertyFindReq oReq);

        #endregion

        #region BrandArticle

        [OperationContract]
        ArticleIDGetResp GetArticleID(int oReq);

        [OperationContract]
        ArticleIDGetResp GetArticleIDs(List<int> oReq);

        [OperationContract]
        ArticleIDGetResp GetAllArticleIDs();


        [OperationContract]
        BrandArticleImageGetResp BrandArticleModelImageGet(BrandArticleModelImageGetReq oReq);

        [OperationContract]
        int GetContractDuration(int Id);

        [OperationContract]
        string GetUomId(string ArticleId, int ContractDuration, string contractID, string offerName);

        #endregion

        #region Get IMOPSDetails By RegID

        //[OperationContract]
        //IMPOSDetailsResp GetIMOPSDetails(int RegID);

        #endregion

        #region Get WhiteListDetails By UserICNO

        [OperationContract]
        WhiteListDetailsResp getWhilstDescreption(string UserICNO);

        #endregion

        #region Get SIMModels Added by VLT on 09 Apr 2013

        [OperationContract]
        SimModelTypeGetResp getSIMModels();
        #endregion

        [OperationContract]
        SimReplacementReasonGetResp GetSimReplacementReason(bool accType);

        [OperationContract]
        SimReplacementReasonResp GetSimReplacementReasonDetails(int ReasonId);

        [OperationContract]
        SimModels GetSimModelDetails(int id, int articalId);

        #region Get MOCOfferDetails By OfferID

        [OperationContract]
        MOCOfferDetailsRes GetMOCOfferDetails(int OfferID);

        [OperationContract]
        List<int> GetOfferID(string ArticleId);

        [OperationContract]
        string GetOfferNameById(int offerID);


        #endregion

        [OperationContract]
        lnkofferuomarticlepriceResp GetOffersfromUOMArticle(string ArticleId, string planID, string contractID, string MOCStatus);

        [OperationContract]
        lnkofferuomarticlepriceResp GetOffersfromUOMArticle2(string ArticleId, string PackageKenanId, string MOCStatus, string contractKenanIDs);

        [OperationContract]
        lnkofferuomarticlepriceResp GetOffersfromUOMArticleNew(string ArticleId, string PkgKenanID, string kenanIds, string MOCStatus, int AcctCtg, int MktCode);


        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid
        [OperationContract]
        List<string> GetContractsForArticleId(string ArticleId, string planID, string mocStatus);

        [OperationContract]
        List<string> GetPackagesForArticleId(string ArticleId, string mocStatus);
        //Added by VLT on 12 May 2013 to get packages/contracts based on artcileid

        [OperationContract]
        List<int> GetContractIdByRegID(int regID);

        [OperationContract]
        List<int> GetContractByRegIdForSmart(int regID);

        [OperationContract]
        int GetContractIdByRegIDSeco(int regID);

        [OperationContract]
        int GetContractIdByRegSuppLineID(int regID);

        [OperationContract]
        string GetContractByCode(string compCode);

        //Added by VLT on 17 May 2013 to get device image details dynamically

        [OperationContract]
        BrandArticleGetResp BrandArticleGet(string articleId);

        [OperationContract]
        GetDeviceImageDetailsResp GetDeviceImageDetails(string articleId, int modelId);


        [OperationContract]
        DiscountPricesRes GetDiscountPrices(int Modelid, int planid, string contractid, int dataplanid);


        //Added by VLT on 17 May 2013 to get device image details dynamically
        #region "Extra Ten"
        [OperationContract]
        long SaveAddRemoveVAS(AddRemoveVASReq oReq);

        [OperationContract]
        int UpdateAddRemoveVAS(AddRemoveVASReq oReq);

        [OperationContract]
        int DeleteAddRemoveVasComponent(int Id);

        [OperationContract]
        int DeleteAddRemoveVasComponents(int regId);

        [OperationContract]
        AddRemoveVASResp GetAddRemoveVASComponents(int regId);


        #endregion

        [OperationContract]
        PgmBdlPckComponentGetResp PgmBdlPckComponentGetForSmartByKenanCode(List<string> oReq);

        [OperationContract]
        List<Online.Registration.DAL.Models.ModelPackageInfo> GetModelPackageInfo_smart(int ModelId, int packageId, string KenanCode,bool isSmart=false);

        //[OperationContract]
        //RegSmartComponentsResp GetSelectedComponents(string regId);




        [OperationContract]
        ExtendPlanFindResp ExtendPlan(ExtendPlanFindReq oReq);
        [OperationContract]
        ExtendBrandFindResp ExtendBrand(ExtendBrandFindReq oReq);
        [OperationContract]
        List<Brand> GetCRPBrands(string type);
        [OperationContract]
        List<Model> GetCrpModels(int Brandid, string type);
        [OperationContract]
        List<ModelGroup> GetCrpPackages(int Modelid, string type);
        [OperationContract]
        List<int> GetExtendedPackages(int Modelid);
        [OperationContract]
        List<Model> GetExtendedModelsDevice(int brandid, string type);
        [OperationContract]
        List<RegSmartComponents> GetSelectedComponents(string regId);

        [OperationContract]
        List<RegSmartComponents> GetSelectedComponentsForCRPPlan(string regId);

        [OperationContract]
        CRPIMPOSDetailsResp GetIMOPSDetails(int RegID);

        [OperationContract]
        List<ContractDataplan> GetContractDataPlan(int DataPlanID);
        
        [OperationContract]
        List<Brand> GetBrandsForPackage(string packageId,string planType);

        [OperationContract]
        List<Model> GetModelsForPackage(int Brandid, string packageId, string planType);

        [OperationContract]
        List<BrandArticle> GetDeviceListForPackage(string packageId, string mktCode, string acctCategory);

        [OperationContract]
        List<Model> GetModelsForExtenstionForPackage(int Brandid, string packageId, string planType);

        [OperationContract]
        List<Brand> GetBrandsForSuppLines();
        [OperationContract]
        List<Model> GetModelsForSupplines(int Brandid);
        [OperationContract]
        List<Model> GetModelsForExtenstionForSupplines(int Brandid);
        [OperationContract]
        List<ModelGroup> GetModelGroupsForSupplines(int Modelid);
        [OperationContract]
        List<int> GetExtendedPackagesForSupplines(int Modelid);

        [OperationContract]
        RestrictedComponentsResp GetAllRestrictedComponents();

        //Changes related to Multiple Contracts CR changed by code--Chetan/ db--Pavan
        [OperationContract]
        List<PlanPrices> GetPlanPrice(int _modelID, string _uom);

        [OperationContract]
        PgmBdlPckComponentGetResp PgmBdlPckComponentGet2(int componentId, int planId);


        #region smart related methods

        [OperationContract]
        IMPOSDetailsResp GetIMOPSDetails_Smart(int RegID);

        [OperationContract]
        PgmBdlPckComponentFindRespSmart PgmBdlPckComponentFindSmart(PgmBdlPckComponentFindReqSmart oReq);

        [OperationContract]
        SmartRebateRes GetSmartRebate(SmartRebateReq oReq);

        [OperationContract]
        PgmBdlPckComponentGetRespSmart PgmBdlPckComponentGetForSmart(PgmBdlPckComponentGetReqSmart oReq);

        [OperationContract]
        ModelGroupModelGetResp_Smart ModelGroupModelGet_Smart(ModelGroupModelGetReq_Smart oReq);

        [OperationContract]
        ModelGroupModelFindResp_Smart ModelGroupModelFind_Smart(ModelGroupModelFindReq_Smart oReq);

        [OperationContract]
        ArticleIDGetRespSmart GetArticleIDSmart(int oReq);

        [OperationContract]
        ArticleIDGetRespSmart GetArticleIDsSmart(List<int> oReq);

        [OperationContract]
        BrandArticleImageGetRespSmart BrandArticleModelImageGetSmart(BrandArticleModelImageGetReqSmart oReq);

        [OperationContract]
        BrandGetRespSmart BrandGetSmart(BrandGetReqSmart oReq);

        [OperationContract]
        BrandFindRespSmart BrandFindSmart(BrandFindReqSmart oReq);

        [OperationContract]
        GetDeviceImageDetailsRespSmart GetDeviceImageDetailsForSmart(string articleId, int modelId);

        [OperationContract]
        ModelGetRespSmart ModelGetSmart(ModelGetReq oReq);

        [OperationContract]
        ModelFindResp ModelFindSmart(ModelFindReqSmart oReq);

        [OperationContract]
        List<BrandSmart> GetBrandSmartsForSuppLines();

        [OperationContract]
        List<ModelGroup> GetModelGroupSmartForSupplines(int Modelid);

        [OperationContract]
        List<ModelGroup> GetCrpSmartPackages(int Modelid, string type);
        
        [OperationContract]
        List<ModelSmart> GetSmartModelsForSupplines(int Brandid);

        [OperationContract]
        int GetContractDurationByKenanCode(string kenanCode);

        [OperationContract]
        List<int> GetSmartExtendedPackages(int Modelid);

        #endregion

        [OperationContract]
        VoiceContractDetailsGetResp GetVoiceAddedendums(VoiceContractDetailsReqStr oReq);
            
        [OperationContract]
        List<string> GetDataContractByRegId(int regID);

        [OperationContract]
        Dictionary<string, string> RetriveDCGreater2GB();
        //CR for auto selection for Discount/Promotion component implementation by chetan
        [OperationContract]
        List<string> GetKenanCodeForReassign();

        [OperationContract]
        Dictionary<string, string> FindPackageOnKenanCode(string KenanCode);

        [OperationContract]
        List<IMPOSDetails> GetPosKeyandOrgIDNew(int RegID);

        [OperationContract]
        IMPOSSpecDetails GetIMPOSSpecDetails(string PosKey);

        [OperationContract]
        IMPOSDetails getIMPOSDetails_Smart(int RegID, string OrganisationId, string strSIMType);

        [OperationContract]
        int GetArticleIdById(int SIMId);

        [OperationContract]
        int UpdateIMPOSDetails(int RegID, string IMPOSFileName, int IMPOSStatus, string strSIMType = "P");

        [OperationContract]
        IMPOSDetails GetIMPOSDetails(int RegID, string OrganisationId, string strSIMType);

        [OperationContract]
        string GetUOMCodeBySIMModelID(int SIMId);

        [OperationContract]
        IMPOSDetails getIMPOSDetailsForPlanOnly(int RegID, string OrganisationId, string strSIMType = "P");

        [OperationContract]
        List<SimModels> GetModelName(int ArticleId, string simCardType);

        [OperationContract]
        DeviceCapacityGetResp GetDeviceCapacity();

        [OperationContract]
        DeviceTypesGetResp GetDeviceType();

        [OperationContract]
        PriceRangeGetResp GetPriceRange();

        [OperationContract]
        SimCardTypesGetResp GetSIMCardTypes();
 	
        [OperationContract]
        List<lnkAcctMktCodePackages> GetPlansbyMarketandAccountCategory(int AcctCategory, int MarketCode);

		[OperationContract]
		List<lnkAcctMktCodeOffers> GetOffersbyMarketandAccountCategory(int AcctCategory, int MarketCode);

        [OperationContract]
        List<Region> GetRegion();

		[OperationContract]
		List<DeviceFinancingInfo> GetAllDeviceFinancingInfo();

		[OperationContract]
		List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleIDs(List<string> _articleIDs);

		[OperationContract]
		List<DeviceFinancingInfo> GetDeviceFinancingInfoByArticleID(string _articleID, int planID);

		#region Plan Device VAS Validation
		[OperationContract]
		PlanDeviceValidationRulesGetResp PlanDeviceValidationRulesGet(PlanDeviceValidationRulesGetReq oReq);
		#endregion
    
        [OperationContract]
        List<refLovList> GetLovList(string lovtype);

		[OperationContract]
		List<refLovList> GetAllLovList();

        [OperationContract]
        List<Accessory> GetAccessoryDetailsByEANKey(string eanKey);

        [OperationContract]
        List<Accessory> GetAccessoryDetailsByArticleID(string articleID);

        [OperationContract]
        List<Accessory> GetAccessoryDetailsByName(string _accessoryName);

        [OperationContract]
        string GetEANKeyByArticleID(string articleID);

        [OperationContract]
        List<OfferAccessory> GetOfferAccessoryDetails(List<string> articleIDs);

        [OperationContract]
        List<OfferAccessory> GetAccessoryAvailability(List<OfferAccessory> offerAccessories, int orgID);

		[OperationContract]
		List<AccFinancingInfo> GetAllAccessoriesFinancingInfo();
    }
}
