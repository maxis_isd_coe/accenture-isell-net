﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
    [ServiceContract]
    public interface IComplainService
    {
        #region Complain

        [OperationContract]
        ComplainCreateResp ComplainCreate(ComplainCreateReq request);

        [OperationContract]
        ComplainUpdateResp ComplainUpdate(ComplainUpdateReq request);

        [OperationContract]
        ComplainFindResp ComplainFind(ComplainFindReq request);

        [OperationContract]
        ComplainGetResp ComplainGet(ComplainGetReq request);

        #endregion

        #region Complaint Issues

        [OperationContract]
        ComplaintIssuesCreateResp ComplaintIssuesCreate(ComplaintIssuesCreateReq request);

        [OperationContract]
        ComplaintIssuesUpdateResp ComplaintIssuesUpdate(ComplaintIssuesUpdateReq request);

        [OperationContract]
        ComplaintIssuesFindResp ComplaintIssuesFind(ComplaintIssuesFindReq request);

        [OperationContract]
        ComplaintIssuesGetResp ComplaintIssuesGet(ComplaintIssuesGetReq request);
        
        [OperationContract]
        ComplaintIssuesGetResp ComplaintIssuesList();
        #endregion

        #region Complaint Issues Group

        [OperationContract]
        ComplaintIssuesGroupCreateResp ComplaintIssuesGroupCreate(ComplaintIssuesGroupCreateReq request);

        [OperationContract]
        ComplaintIssuesGroupUpdateResp ComplaintIssuesGroupUpdate(ComplaintIssuesGroupUpdateReq request);

        [OperationContract]
        ComplaintIssuesGroupFindResp ComplaintIssuesGroupFind(ComplaintIssuesGroupFindReq request);

        [OperationContract]
        ComplaintIssuesGroupGetResp ComplaintIssuesGroupGet(ComplaintIssuesGroupGetReq request);

        #endregion

        #region Issue Group Dispatch Queue

        [OperationContract]
        IssueGrpDispatchQCreateResp IssueGrpDispatchQCreate(IssueGrpDispatchQCreateReq request);

        [OperationContract]
        IssueGrpDispatchQUpdateResp IssueGrpDispatchQUpdate(IssueGrpDispatchQUpdateReq request);

        [OperationContract]
        IssueGrpDispatchQFindResp IssueGrpDispatchQFind(IssueGrpDispatchQFindReq request);

        [OperationContract]
        IssueGrpDispatchQGetResp IssueGrpDispatchQGet(IssueGrpDispatchQGetReq request);

        #endregion

        [OperationContract]
        ComplaintGetResp GetCmssList(Online.Registration.DAL.Models.CMSSComplainGet objComplaint);
        
        [OperationContract]
        ComplaintIssuesGetResp GetRequredXMLInputs(ComplaintIssuesGroupFindReq request);
    }
}
