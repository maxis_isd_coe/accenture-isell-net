﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.DAL.Models;
using Online.Registration.Service.Models;

namespace Online.Registration.Service
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUpdateService" in both code and config file together.
	[ServiceContract]
	public interface IUpdateService
	{
		[OperationContract]
		int saveRegMdlGrpModel(RegMdlGrpModel objRegMdlGrpModel);

		[OperationContract]
		int saveRegMdlGrpModels(List<RegMdlGrpModel> objRegMdlGrpModels);

		[OperationContract]
		int uploadDocs(RegUploadDoc objRegUploadDoc);

		[OperationContract]
		CustomerUpdateResp updateTblCustomer(CustomerUpdateReq objTblCustomer);

		[OperationContract]
		int SavePhotoToDB(string photoData, string custPhoto, string altCustPhoto, int regid);

		[OperationContract]
		bool FindDocument(string criteria, int regid);

		[OperationContract]
		List<RegUploadDoc> GetAllDocument(int regID);
	}
}
