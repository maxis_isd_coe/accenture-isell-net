﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Online.Registration.Service.Models;
using Online.Registration.DAL.Models;

namespace Online.Registration.Service
{
    [ServiceContract]
    public interface IConfigService
    {
        #region Market

        [OperationContract]
        MarketCreateResp MarketCreate(MarketCreateReq oReq);
        [OperationContract]
        MarketUpdateResp MarketUpdate(MarketUpdateReq oReq);
        [OperationContract]
        MarketFindResp MarketFind(MarketFindReq oReq);
        [OperationContract]
        MarketSearchResp MarketSearch(MarketFindReq oReq);
        [OperationContract]
        MarketGetResp MarketGet(MarketGetReq oReq);
        [OperationContract]
        MarketGetResp MarketList();

        #endregion

        #region AccountCategory

        [OperationContract]
        AccountCategoryCreateResp AccountCategoryCreate(AccountCategoryCreateReq oReq);
        [OperationContract]
        AccountCategoryUpdateResp AccountCategoryUpdate(AccountCategoryUpdateReq oReq);
        [OperationContract]
        AccountCategoryFindResp AccountCategoryFind(AccountCategoryFindReq oReq);
        [OperationContract]
        AccountCategoryGetResp AccountCategoryGet(AccountCategoryGetReq oReq);
        [OperationContract]
        AccountCategoryGetResp AccountCategoryList();

        #endregion

        #region Status
        [OperationContract]
        StatusCreateResp StatusCreate(StatusCreateReq oReq);
        [OperationContract]
        StatusUpdateResp StatusUpdate(StatusUpdateReq oReq);
        [OperationContract]
        StatusGetResp StatusGet(StatusGetReq oReq);
        [OperationContract]
        StatusGetResp StatusList();
        [OperationContract]
        StatusFindResp StatusFind(StatusFindReq oReq);
        /* Added by Patanjali on 01-04-2013 to support find for store keeper and cashier */
        [OperationContract]
        StatusFindResp StoreKeeperFind(StatusFindReq oReq);
        [OperationContract]
        StatusFindResp CashierFind(StatusFindReq oReq);
        /* Added by Patanjali on 01-04-2013 to support find for store keeper and cashier Ends here */
        #endregion

        #region StatusType
        [OperationContract]
        StatusTypeCreateResp StatusTypeCreate(StatusTypeCreateReq oReq);
        [OperationContract]
        StatusTypeUpdateResp StatusTypeUpdate(StatusTypeUpdateReq oReq);
        [OperationContract]
        StatusTypeGetResp StatusTypeGet(StatusTypeGetReq oReq);
        [OperationContract]
        StatusTypeGetResp StatusTypeList();
        [OperationContract]
        StatusTypeFindResp StatusTypeFind(StatusTypeFindReq oReq);
        #endregion

        #region Status Change
        [OperationContract]
        StatusChangeCreateResp StatusChangeCreate(StatusChangeCreateReq oReq);
        [OperationContract]
        StatusChangeUpdateResp StatusChangeUpdate(StatusChangeUpdateReq oReq);
        [OperationContract]
        StatusChangeGetResp StatusChangeGet(StatusChangeGetReq oReq);
        [OperationContract]
        StatusChangeFindResp StatusChangeFind(StatusChangeFindReq oReq);
        #endregion

        #region Status Reason
        [OperationContract]
        StatusReasonCreateResp StatusReasonCreate(StatusReasonCreateReq oReq);
        [OperationContract]
        StatusReasonUpdateResp StatusReasonUpdate(StatusReasonUpdateReq oReq);
        [OperationContract]
        StatusReasonGetResp StatusReasonGet(StatusReasonGetReq oReq);
        [OperationContract]
        StatusReasonGetResp StatusReasonList();
        [OperationContract]
        StatusReasonFindResp StatusReasonFind(StatusReasonFindReq oReq);
        #endregion

        #region Status Reason Code
        [OperationContract]
        StatusReasonCodeCreateResp StatusReasonCodeCreate(StatusReasonCodeCreateReq oReq);
        [OperationContract]
        StatusReasonCodeUpdateResp StatusReasonCodeUpdate(StatusReasonCodeUpdateReq oReq);
        [OperationContract]
        StatusReasonCodeGetResp StatusReasonCodeGet(StatusReasonCodeGetReq oReq);
        [OperationContract]
        StatusReasonCodeFindResp StatusReasonCodeFind(StatusReasonCodeFindReq oReq);
        [OperationContract]
        StatusReasonCodeIDsFindResp StatusReasonCodeIDsFind();
        #endregion

        #region smart related methods

        [OperationContract]
        TransactionTypeGetResp TransactionTypeGet();

        #endregion

        #region BRE Check
        [OperationContract]
        List<BRECheck> GetBRECheckDetails(List<int> userGroupId);
        #endregion

    }
}
