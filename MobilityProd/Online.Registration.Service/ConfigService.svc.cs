﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using log4net;

using SNT.Utility;

using Online.Registration.Service.Models;
using Online.Registration.DAL.Admin;

using log4net;
using Online.Registration.DAL.Models;

namespace Online.Registration.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ConfigService" in code, svc and config file together.
    public class ConfigService : IConfigService
    {
        //private static readonly ILog Logger = LogManager.GetLogger("ConfigService");
        static string svcName = "ConfigService";
        private static readonly ILog Logger = LogManager.GetLogger(svcName);
        public ConfigService()
        {
            //if (log4net.LogManager.GetCurrentLoggers().Count() == 0)
            //{
            //    log4net.Config.XmlConfigurator.Configure();
            //}
            Logger.InfoFormat("{0} initialized....", svcName);
        }

        #region Private Methods

        private void PopulateExceptionResponse(WCFResponse resp, Exception ex)
        {
            resp.Code = "-1";
            resp.Message = ex.Message;
            resp.StackTrace = ex.StackTrace;
        }

        #endregion

        #region Market

        public MarketCreateResp MarketCreate(MarketCreateReq request)
        {
            var response = new MarketCreateResp();

            try
            {
                response.ID = OnlineRegAdmin.MarketCreate(request.Market);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public MarketUpdateResp MarketUpdate(MarketUpdateReq request)
        {
            var response = new MarketUpdateResp();

            try
            {
                OnlineRegAdmin.MarketUpdate(request.Market);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public MarketFindResp MarketFind(MarketFindReq request)
        {
            var response = new MarketFindResp();

            try
            {
                response.IDs = OnlineRegAdmin.MarketFind(request.MarketFind);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public MarketSearchResp MarketSearch(MarketFindReq request)
        {
            var response = new MarketSearchResp();

            try
            {
                response.Markets = OnlineRegAdmin.MarketSearch(request.MarketFind);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public MarketGetResp MarketGet(MarketGetReq request)
        {
            var response = new MarketGetResp();

            try
            {
                response.Markets = OnlineRegAdmin.MarketGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public MarketGetResp MarketList()
        {
            var response = new MarketGetResp();
            try
            {
                response.Markets = OnlineRegAdmin.MarketList();
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }
            return response;
        }
        #endregion

        #region Account Category

        public AccountCategoryCreateResp AccountCategoryCreate(AccountCategoryCreateReq request)
        {
            var response = new AccountCategoryCreateResp();

            try
            {
                response.ID = OnlineRegAdmin.AccountCategoryCreate(request.AccountCategory);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public AccountCategoryUpdateResp AccountCategoryUpdate(AccountCategoryUpdateReq request)
        {
            var response = new AccountCategoryUpdateResp();

            try
            {
                OnlineRegAdmin.AccountCategoryUpdate(request.AccountCategory);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public AccountCategoryFindResp AccountCategoryFind(AccountCategoryFindReq request)
        {
            var response = new AccountCategoryFindResp();

            try
            {
                response.IDs = OnlineRegAdmin.AccountCategoryFind(request.AccountCategoryFind);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public AccountCategoryGetResp AccountCategoryGet(AccountCategoryGetReq request)
        {
            var response = new AccountCategoryGetResp();

            try
            {
                response.AccountCategories = OnlineRegAdmin.AccountCategoryGet(request.IDs);
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }

            return response;
        }

        public AccountCategoryGetResp AccountCategoryList()
        {
            var response = new AccountCategoryGetResp();
            try
            {
                response.AccountCategories = OnlineRegAdmin.AccountCategoryList();
            }
            catch (Exception ex)
            {
                Logger.Error(" Error: " + Util.LogException(ex));
                PopulateExceptionResponse(response, ex);
            }
            return response;
        }

        #endregion

        #region Status
        public StatusCreateResp StatusCreate(StatusCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusCreate({1})", svcName, oReq.Status.Description);
            StatusCreateResp oRp = new StatusCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StatusCreate(oReq.Status);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg="", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusCreate({1}): {2}|{3}", svcName, oReq.Status.Description, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusUpdateResp StatusUpdate(StatusUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusUpdate({1})", svcName, oReq.Status.Description);
            StatusUpdateResp oRp = new StatusUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StatusUpdate(oReq.Status);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Status update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg="", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusUpdate({1}): {2}", svcName, oReq.Status.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusGetResp StatusGet(StatusGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            StatusGetResp oRp = new StatusGetResp();
            try
            {
                oRp.Statuss = OnlineRegAdmin.StatusGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public StatusGetResp StatusList()
        {
            Logger.InfoFormat("Entering {0}.StatusList({1})",svcName,"" );
            StatusGetResp oRp = new StatusGetResp();
            try
            {
                oRp.Statuss = OnlineRegAdmin.StatusList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public StatusFindResp StatusFind(StatusFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, oReq.StatusFind);
            StatusFindResp oRp = new StatusFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusFind(oReq.StatusFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, oReq.StatusFind.Status.Description, oRp.Code);
            }
            return oRp;
        }
        #region Added by Patanjali to add find for Store Keeper and Cashier
        
        public StatusFindResp StoreKeeperFind(StatusFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, oReq.StatusFind);
            StatusFindResp oRp = new StatusFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StoreKeeperFind(oReq.StatusFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, oReq.StatusFind.Status.Description, oRp.Code);
            }
            return oRp;
        }

        public StatusFindResp CashierFind(StatusFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusFind({1})", svcName, oReq.StatusFind);
            StatusFindResp oRp = new StatusFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.CashierFind(oReq.StatusFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusFind({1}): {2}", svcName, oReq.StatusFind.Status.Description, oRp.Code);
            }
            return oRp;
        }

        #endregion
        #endregion

        #region StatusType
        public StatusTypeCreateResp StatusTypeCreate(StatusTypeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeCreate({1})", svcName, oReq.StatusType.Description);
            StatusTypeCreateResp oRp = new StatusTypeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StatusTypeCreate(oReq.StatusType);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                oRp.ID = 0;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusTypeCreate({1}): {2}|{3}", svcName, oReq.StatusType.Description, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusTypeUpdateResp StatusTypeUpdate(StatusTypeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeUpdate({1})", svcName, oReq.StatusType.Description);
            StatusTypeUpdateResp oRp = new StatusTypeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StatusTypeUpdate(oReq.StatusType);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "StatusType update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                //oRp.Message = ex.Message;
                //oRp.StackTrace = ex.StackTrace;

                string errmsg = "", stacktrace = "";
                //TCMReg.DAL.Workers.ExceptionHelper.GetAllExceptions(ex, out errmsg="", out stacktrace, false);
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusTypeUpdate({1}): {2}", svcName, oReq.StatusType.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusTypeGetResp StatusTypeGet(StatusTypeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            StatusTypeGetResp oRp = new StatusTypeGetResp();
            try
            {
                oRp.StatusTypes = OnlineRegAdmin.StatusTypeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusTypeGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public StatusTypeGetResp StatusTypeList()
        {
            Logger.InfoFormat("Entering {0}.StatusTypeList({1})", svcName, "");
            StatusTypeGetResp oRp = new StatusTypeGetResp();
            try
            {
                oRp.StatusTypes = OnlineRegAdmin.StatusTypeList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusTypeList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }

        public StatusTypeFindResp StatusTypeFind(StatusTypeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusTypeFind({1})", svcName, oReq.StatusTypeFind);
            StatusTypeFindResp oRp = new StatusTypeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusTypeFind(oReq.StatusTypeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusTypeFind({1}): {2}", svcName, oReq.StatusTypeFind.StatusType.Description, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Status Change
        public StatusChangeCreateResp StatusChangeCreate(StatusChangeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeCreate({1})", svcName, oReq.StatusChange.FromStatusID);
            StatusChangeCreateResp oRp = new StatusChangeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StatusChangeCreate(oReq.StatusChange);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.ID = 0;

                string errmsg = "", stacktrace = "";

                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusChangeCreate({1}): {2}|{3}", svcName, oReq.StatusChange.FromStatusID, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusChangeUpdateResp StatusChangeUpdate(StatusChangeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeUpdate({1})", svcName, oReq.StatusChange.FromStatusID);
            StatusChangeUpdateResp oRp = new StatusChangeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StatusChangeUpdate(oReq.StatusChange);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Status Change update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";

                string errmsg = "", stacktrace = "";
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusChangeUpdate({1}): {2}", svcName, oReq.StatusChange.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusChangeGetResp StatusChangeGet(StatusChangeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            StatusChangeGetResp oRp = new StatusChangeGetResp();
            try
            {
                oRp.StatusChanges = OnlineRegAdmin.StatusChangeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusChangeGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public StatusChangeFindResp StatusChangeFind(StatusChangeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusChangeFind({1})", svcName, oReq.StatusChangeFind);
            StatusChangeFindResp oRp = new StatusChangeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusChangeFind(oReq.StatusChangeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusChangeFind({1}): {2}", svcName, oReq.StatusChangeFind, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Status Reason
        public StatusReasonCreateResp StatusReasonCreate(StatusReasonCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCreate({1})", svcName, oReq.StatusReason.Code);
            StatusReasonCreateResp oRp = new StatusReasonCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StatusReasonCreate(oReq.StatusReason);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.ID = 0;

                string errmsg = "", stacktrace = "";

                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonCreate({1}): {2}|{3}", svcName, oReq.StatusReason.Code, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonUpdateResp StatusReasonUpdate(StatusReasonUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonUpdate({1})", svcName, oReq.StatusReason.Code);
            StatusReasonUpdateResp oRp = new StatusReasonUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StatusReasonUpdate(oReq.StatusReason);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Status Reason update fail";

                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";

                string errmsg = "", stacktrace = "";
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonUpdate({1}): {2}", svcName, oReq.StatusReason.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonGetResp StatusReasonGet(StatusReasonGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            StatusReasonGetResp oRp = new StatusReasonGetResp();
            try
            {
                oRp.StatusReasons = OnlineRegAdmin.StatusReasonGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }

        public StatusReasonGetResp StatusReasonList()
        {
            Logger.InfoFormat("Entering {0}.StatusReasonList({1})", svcName,"");
            StatusReasonGetResp oRp = new StatusReasonGetResp();
            try
            {
                oRp.StatusReasons = OnlineRegAdmin.StatusReasonList();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonList({1}): {2}", svcName, "", oRp.Code);
            }
            return oRp;
        }
        public StatusReasonFindResp StatusReasonFind(StatusReasonFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonFind({1})", svcName, oReq.StatusReasonFind);
            StatusReasonFindResp oRp = new StatusReasonFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusReasonFind(oReq.StatusReasonFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonFind({1}): {2}", svcName, oReq.StatusReasonFind.StatusReason.Code, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region Status Reason Code
        public StatusReasonCodeCreateResp StatusReasonCodeCreate(StatusReasonCodeCreateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeCreate({1})", svcName, oReq.StatusReasonCode.StatusID);
            StatusReasonCodeCreateResp oRp = new StatusReasonCodeCreateResp();
            try
            {
                oRp.ID = OnlineRegAdmin.StatusReasonCodeCreate(oReq.StatusReasonCode);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.ID = 0;

                string errmsg = "", stacktrace = "";

                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonCodeCreate({1}): {2}|{3}", svcName, oReq.StatusReasonCode.StatusID, oRp.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonCodeUpdateResp StatusReasonCodeUpdate(StatusReasonCodeUpdateReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeUpdate({1})", svcName, oReq.StatusReasonCode.StatusID);
            StatusReasonCodeUpdateResp oRp = new StatusReasonCodeUpdateResp();
            int value = 0;
            try
            {
                value = OnlineRegAdmin.StatusReasonCodeUpdate(oReq.StatusReasonCode);
                if (value == 0)
                {
                    oRp.Code = "-1";
                    oRp.Message = "Status Reason Code update fail";
                }
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";

                string errmsg = "", stacktrace = "";
                oRp.Message = errmsg;
                oRp.StackTrace = stacktrace;
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg = "", stacktrace), ex);
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonCodeUpdate({1}): {2}", svcName, oReq.StatusReasonCode.ID, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonCodeGetResp StatusReasonCodeGet(StatusReasonCodeGetReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeGet({1})", svcName, oReq.IDs != null && oReq.IDs.Count() != 0 ? oReq.IDs[0] : 0);
            StatusReasonCodeGetResp oRp = new StatusReasonCodeGetResp();
            try
            {
                oRp.StatusReasonCodes = OnlineRegAdmin.StatusReasonCodeGet(oReq.IDs);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonCodeGet({1}): {2}", svcName, (oReq.IDs != null && oReq.IDs.Count() != 0) ? oReq.IDs[0] : 0, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonCodeFindResp StatusReasonCodeFind(StatusReasonCodeFindReq oReq)
        {
            Logger.InfoFormat("Entering {0}.StatusReasonCodeFind({1})", svcName, oReq.StatusReasonCodeFind);
            StatusReasonCodeFindResp oRp = new StatusReasonCodeFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusReasonCodeFind(oReq.StatusReasonCodeFind);
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.StatusReasonCodeFind({1}): {2}", svcName, oReq.StatusReasonCodeFind.StatusReasonCode.StatusID, oRp.Code);
            }
            return oRp;
        }
        public StatusReasonCodeIDsFindResp StatusReasonCodeIDsFind()
        {
            //Logger.InfoFormat("Entering {0}.StatusReasonCodeFind({1})", svcName, oReq.StatusReasonCode);
            StatusReasonCodeIDsFindResp oRp = new StatusReasonCodeIDsFindResp();
            try
            {
                oRp.IDs = OnlineRegAdmin.StatusReasonCodeIDsFind();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {
                //Logger.InfoFormat("Exiting {0}.StatusReasonCodeFind({1}): {2}", svcName, oReq.StatusReasonCodeFind.StatusReasonCode.StatusID, oRp.Code);
            }
            return oRp;
        }
        #endregion

        #region smart related methods
        public TransactionTypeGetResp TransactionTypeGet()
        {

            TransactionTypeGetResp oRp = new TransactionTypeGetResp();
            try
            {
                oRp.Statuss = OnlineRegAdmin.TransactionTypeGet();
            }
            catch (Exception ex)
            {
                oRp.Code = "-1";
                oRp.Message = ex.Message;
                oRp.StackTrace = ex.StackTrace;
                Logger.Error(" Error: " + Util.LogException(ex));
            }
            finally
            {

            }
            return oRp;
        }
        #endregion

        #region BRE Check
        public List<BRECheck> GetBRECheckDetails(List<int> userGroupId)
        {
            Logger.InfoFormat("Entering {0}.GetBRECheckDetails()({1})", svcName, "");
            List<BRECheck> objBREConfiguration = new List<BRECheck>();
            try
            {
                objBREConfiguration = OnlineRegAdmin.GetBRECheckDetails(userGroupId).ToList();
                    
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", ex.Message, ex.StackTrace), ex);
                string errmsg = "", stacktrace = "";
                Logger.Error(string.Format("ErrorMessage: {0}| StackTrace: {1}", errmsg, stacktrace), ex);

            }
            finally
            {
                Logger.InfoFormat("Exiting {0}.GetBRECheckDetails({1}):", svcName, "");
            }

            return objBREConfiguration;

        }
        #endregion
    }
}
