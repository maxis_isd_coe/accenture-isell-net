﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.Service.Tests.SRService;
using Online.Registration.DAL.Models.Common;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Repositories;
using System.Collections.Generic;

namespace Online.Registration.Service.Tests
{
    [TestClass]
    public class CorpSimRepSvcTest
    {
        [TestMethod]
        [TestCategory("SRServiceTest")]
        public void CorpSimRep_Submit_Retrieve_Should_Success()
        {
            using(var client=new SRServiceClient())
            {
                var order = GetTestOrder();
                var saveOrdeReq = new SRSaveOrderReq() {
                    Order = order
                };
                var saveOrderResp = client.SaveOrder(saveOrdeReq);
                //Save successful
                Assert.IsTrue(!string.IsNullOrEmpty((string)saveOrderResp.Content),"Save fail");
                var findOrderReq = new SRFindOrderReq() {
                    OrderId=saveOrderResp.Content.ToString()
                };
                var findOrderResp = client.FindOrderById(findOrderReq);
                var findOrder=findOrderResp.Content as SROrderMaster;
                var findDetail = findOrder.Detail as SROrderCorpSimRepDtl;
                //Retrieve
                Assert.IsNotNull(findOrder, "Order is null");
                Assert.IsTrue(findOrder.VerificationDocs.Count > 0, "Verification is null");
                Assert.IsNotNull(findOrder.CenterOrg, "Organization is null");
                Assert.IsNotNull(findDetail.OrderId, "Order detail null");
                Assert.IsNotNull(findDetail.CustId, "Customer is null");
                //Clean it use repository as not use in wcf
                var repo = new SRRepository();
                repo.DeleteOrderById(saveOrderResp.Content.ToString());
            }
        }

        private SROrderMaster GetTestOrder()
        {
            var order = new SROrderMaster()
            {
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                RequestType = RequestTypes.CorpSimReplace,
                CenterOrgId=269,
                Status = SRStatus.New,
            };
            order.VerificationDocs = new List<VerificationDoc>() {
                new VerificationDoc() {File="abc",FileName="abc.jpg",DocType=DocTypes.IdBack ,CreateBy = "ccklcc1",CreateDt=DateTime.Now},
                new VerificationDoc() {File="front",FileName="front.jpg",DocType=DocTypes.IdFront ,CreateBy = "ccklcc1",CreateDt=DateTime.Now}
            };
            var orderDtls = new SROrderCorpSimRepDtl()
            {
                MSISDN = "01996211691",
                AccountNo = "1232323",
                CustBiometricPass = false,
                NewSimSerial = "1234567890123456789",
                SignDt = DateTime.Now,
                Remarks = "something"
            };
            orderDtls.Customer = new Customer()
            {
                IDCardTypeID = 1,
                IDCardNo = "881111111111",
                FullName = "Jazz Tong",
                PayModeID = 1,
                CreateDT = DateTime.Now,
                LastAccessID = "ccklcc1",
                LastUpdateDT = DateTime.Now
            };
            order.Detail = orderDtls;
            return order;
        }
    }
}
