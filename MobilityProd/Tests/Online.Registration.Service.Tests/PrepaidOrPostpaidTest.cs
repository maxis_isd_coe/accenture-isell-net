﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.Service.Tests.SubscriberICServiceTest;
//using Online.Registration.Web.Helper;

namespace Online.Registration.Service.Tests
{
    [TestClass]
    public class PrepaidOrPostpaidTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Console.WriteLine(GetAccountType("60126519000"));
        }

        public string GetAccountType(string msisdn)
        {
            string ExternalMSISDN = msisdn;
            string AccountType;
            string CurrentPlan;
            SubscriberRetrieveServiceInfoRequest serviceInfoReq = new SubscriberRetrieveServiceInfoRequest();
            SubscriberRetrieveServiceInfoResponse serviceInfoResp = new SubscriberRetrieveServiceInfoResponse();
            SubscriberICServiceClient serviceInfoClient = new SubscriberICServiceClient();
            CustomizedCustomer customer = new CustomizedCustomer();

            serviceInfoReq.externalId = msisdn;

            serviceInfoResp = serviceInfoClient.retrieveServiceInfo(serviceInfoReq);
            AccountType = serviceInfoResp.postPreInd;
            CurrentPlan = serviceInfoResp.packageName;

            //string Exception = null;
            //var output = serviceInfoClient.retrieveAcctListByIC("New IC", "780114105995", "svklcc1", serviceException: ref Exception);

            return AccountType;
        }
    }
}
