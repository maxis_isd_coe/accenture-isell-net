﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Online.Registration.Service.Tests.SRService;
using System.Linq;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models.FilterCriterias;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.Service.Tests
{
    [TestClass]
    public class SRDashboardSvcTest
    {
        [TestMethod]
        public void QueryDistinctCreateByList()
        {
            using(var client=new SRService.SRServiceClient())
            {
                //var req = new SRFindOrderReq() { OrderId = "SR100288" };
                //var resp2 = client.FindOrderById(req);
                var resp = client.GetDistinctCreateByList(new SRServiceReq());
                var lst = resp.Content as CollectionContract;
                var newLst = lst.Cast<string>().ToList();
                Assert.IsTrue(lst.Count > 0);
            }
        }

        [TestMethod]
        public void SimpleQueryResult(){
            using(var client=new SRService.SRServiceClient()){
                var resp = client.QueryOrder(new SRQueryOrderReq());
                var lst = (resp.Content as CollectionContract).Cast<SRQueryMaster>().ToList();
                Assert.IsTrue(lst.Count > 0, "Simple query fail");
            }
        }

        [TestMethod]
        public void TestQueryWithFilter(){
            var sr = new SRQueryMaster();
            var cri = new SRQueryOrderDto()
            {
                CriteriaList = new List<IFilterCriteria>(){
                    //new FilterCriteria(sr.Name(n=>n.CreateBy),"svklcc1"),
                    //new TodayFilterCriteria(sr.Name(n=>n.CreateDt))
                    new StartDateFilterCriteria(sr.Name(n=>n.CreateDt),new DateTime(2015,11,1)),
                    new EndDateFilterCriteria(sr.Name(n=>n.CreateDt),new DateTime(2015,11,5))
                },
                SortyBy = sr.Name(n => n.FullName),
                SortDesc = false
            };
            using(var client=new SRServiceClient()){
                var resp = client.QueryOrder(new SRQueryOrderReq() {
                    QueryDto=cri
                });
                var lst = (resp.Content as CollectionContract).Cast<SRQueryMaster>().ToList();
                Assert.IsTrue(lst.Count > 0, "Query with Filter fail");
            }
        }
    }
}
