﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using Online.Registration.DAL.Interfaces;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class TestOthers
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void TestSaveRetrieveDeleteAddress()
        {
            var address = new CommonAddress
            {
                Line1 = "8, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active=true
            };
            using (var dbcontext = new OnlineStoreDbContext())
            {
                dbcontext.CommonAddresses.Add(address);
                dbcontext.SaveChanges();
                Assert.IsTrue(address.ID > 0);
                var findAddress = dbcontext.CommonAddresses.Where(n => n.ID == address.ID).SingleOrDefault();
                Assert.IsNotNull(findAddress, "Address cannot retrieve");
                dbcontext.CommonAddresses.Remove(findAddress);
                var deleteCount = dbcontext.SaveChanges();
                Assert.IsTrue(deleteCount > 0, "Delete fail");
            }

        }

        [TestMethod]
        public void TestGetDistinctCreateByList_ShouldReturnData(){
            var lst = repo.GetDistinctCreateByList(269);
            Assert.IsTrue(lst.Count() > 0, "No createby list");
        }
    }
}
