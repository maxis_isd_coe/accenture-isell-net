﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using System.Linq;
using System.Collections.Generic;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class BillSeparationTest
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void CreateNewOrderAndDeleteShouldSuccess()
        {
            var order=new SROrderMaster() {
                CreateDt=DateTime.Now,
                CreateBy="ccklcc1",
                RequestType=RequestTypes.BillSeparation,
                Status=SRStatus.New,
                CenterOrgId=269
            };
            order.VerificationDocs = new List<VerificationDoc>() {
                new VerificationDoc() {File="abc",FileName="abc.jpg",DocType=DocTypes.IdBack ,CreateBy = "ccklcc1",CreateDt=DateTime.Now},
                new VerificationDoc() {File="front",FileName="front.jpg",DocType=DocTypes.IdFront ,CreateBy = "ccklcc1",CreateDt=DateTime.Now}
            };
            var orderDtls= new SROrderBillSeparationDtl() {
                CustMSISDN="01996211691",
                CustAcctNumber="1232323",
                CustBiometricPass=false,
                SignDt=DateTime.Now,
                Remarks="something",
                CustCurrentPlan="Maxis One Plan",
                ExistMSISDN="0126519002",
                ExistAcctNumber="1234567890",
            };
            orderDtls.Customer = new Customer() {
                IDCardTypeID=1,
                IDCardNo="881111111111",
                FullName="Jazz Tong",
                PayModeID=1,
                CreateDT=DateTime.Now,
                LastAccessID="ccklcc1",
                LastUpdateDT=DateTime.Now
            };
            orderDtls.CurrAddress = new CommonAddress{
                Line1 = "8, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active=true
            };
            orderDtls.NewAddress = new CommonAddress
            {
                Line1 = "6, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active = true
            };
            order.Detail = orderDtls;

            //Test save
            var result = repo.SaveOrder(order);
            Assert.IsNotNull(result);
            var findResult = repo.FindOrderById(order.OrderId);
            //Test retrieve
            Assert.AreEqual(order.ID, findResult.ID);
            //Test other sub data load
            Assert.IsTrue(order.VerificationDocs.Count > 0, "Verification");
            Assert.IsNotNull(findResult.Detail, "Detail");
            Assert.IsNotNull((findResult.Detail as SROrderBillSeparationDtl).Customer, "Customer");
            Assert.IsNotNull(findResult.CenterOrg, "CenterOrg");
            //Test delete
            var deleteResult = repo.DeleteOrderById(order.OrderId);
            var deleteAndFindResult = repo.FindOrderById(order.OrderId);
            Assert.IsNull(deleteAndFindResult, "Delete result fail");
        }
    }
}
