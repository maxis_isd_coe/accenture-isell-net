﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using System.Linq;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class TransferOwnerTest
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void CreateNewOrderAndDeleteShouldSuccess()
        {
            var order=new SROrderMaster() {
                CreateDt=DateTime.Now,
                CreateBy="ccklcc1",
                RequestType=RequestTypes.TransferOwnerShip,
                Status=SRStatus.New,
                CenterOrgId=269
            };
            var orderDtls= new SROrderTransferOwnerDtl() {
                //TransferorName="Ashley Ow",
                //TransferorIDType="New IC",
                //TransferorIDNumber="780114105995",
                TransferorMSISDN ="01996211691",
                TransferorAcctNumber = "1232323",
                Transferor_CustBiometricPass=false,
                Transferee_CustBiometricPass=false,
                SignDt=DateTime.Now,
                Remarks="something",
                //TransfereeName="Jazz Tong",
                //TransfereeIDType="New IC",
                //TransfereeIDNumber="880123101234",
                //TransfereeDOB=DateTime.Now,
                //TransfereeEmail="jazztong@maxis.com.my",
                //TransfereeLanguage="Chinese",
                //TransfereeNationality="Malaysian",
                //TransfereeRace="Chinese",
                //TransfereeSalutation="Mr.",
                //TransfereeGender="Male",
                TransfereeMSISDN="60126529002",
                TransfereeAcctNumber="1234567890",
                //TransfereeContactNumber="60126519002",
                //TransfereeAltContactNumber="60126529002"
            };
            orderDtls.TransferorCustomer = new Customer() {
                IDCardTypeID=1,
                IDCardNo="881111111111",
                FullName="Jazz Tong",
                PayModeID=1,
                CreateDT=DateTime.Now,
                LastAccessID="ccklcc1",
                LastUpdateDT=DateTime.Now
            };
            orderDtls.TransferorAddress = new CommonAddress()
            {
                Line1 = "8, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active = true
            };
            orderDtls.TransfereeAddress = new CommonAddress()
            {
                Line1 = "8, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active = true
            };
            //orderDtls.VerificationDocs = new VerificationDocs() {
            //    CreateBy="ccklcc1",
            //    CreateDt=DateTime.Now,
            //    IdFrontFileName="something.jpg",
            //    Justification="It is very important person"
            //};
            order.Detail = orderDtls;

            //Test save
            var result = repo.SaveOrder(order);
            Assert.IsNotNull(result);
            var findResult = repo.FindOrderById(order.OrderId);
            //Test retrieve
            Assert.IsNotNull(findResult.Detail, "Detail");
            Assert.IsNotNull((findResult.Detail as SROrderTransferOwnerDtl).TransferorCustomer, "Customer");
            Assert.IsNotNull((findResult.Detail as SROrderTransferOwnerDtl).TransfereeAddress.ID, "Address");
            Assert.IsNotNull((findResult.Detail as SROrderTransferOwnerDtl).TransferorAddress.ID, "Address");
            //Assert.IsNotNull(findResult.Detail.VerificationDocs, "Verification");
            Assert.IsNotNull(findResult.CenterOrg, "CenterOrg");
            //Test delete
            var deleteResult = repo.DeleteOrderById(order.OrderId);
            Assert.IsTrue(deleteResult > 0);
        }
    }
}
