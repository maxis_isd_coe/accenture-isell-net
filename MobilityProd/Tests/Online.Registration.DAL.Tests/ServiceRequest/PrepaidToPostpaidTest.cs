﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using System.Linq;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class PrepaidToPostpaidTest
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void CreateNewOrderAndDeleteShouldSuccess()
        {
            var order=new SROrderMaster() {
                CreateDt=DateTime.Now,
                CreateBy="ccklcc1",
                RequestType=RequestTypes.Postpaid2Prepaid,
                Status=SRStatus.New,
                CenterOrgId=269
            };
            var orderDtls= new SROrderPreToPostDtl() {
                CustMSISDN = "01996211691",
                PrepaidAccNumber = "1232323",
                CustName = "Ashley Ow",
                CustBiometricPass = false,
                CustIDType = "New IC",
                CustIDNumber = "780114105995",
                SignDt = DateTime.Now,
                Remarks="something",
                PostpaidSIMSerial="1234567890123456789",
                PostpaidPlan="Maxis One Plan",
                PrepaidPlan="Hotlink Plan",
            };
            orderDtls.Customer = new Customer() {
                IDCardTypeID=1,
                IDCardNo="881111111111",
                FullName="Jazz Tong",
                PayModeID=1,
                CreateDT=DateTime.Now,
                LastAccessID="ccklcc1",
                LastUpdateDT=DateTime.Now
            };
            orderDtls.CustAddress = new CommonAddress()
            {
                Line1 = "8, Jalan Adang U8/16",
                Line2 = "Seksyen U8",
                Line3 = "Bukit Jelutong",
                Town = "Shah Alam",
                Postcode = "40150",
                StateID = 14,
                CountryID = 1,
                CreateDt = DateTime.Now,
                CreateBy = "ccklcc1",
                UpdateDt = DateTime.Now,
                UpdateBy = "ccklcc1",
                Active = true
            };
            //orderDtls.VerificationDocs = new VerificationDocs() {
            //    CreateBy="ccklcc1",
            //    CreateDt=DateTime.Now,
            //    IdFrontFileName="something.jpg",
            //    Justification="It is very important person"
            //};
            order.Detail = orderDtls;

            //Test save
            var result = repo.SaveOrder(order);
            Assert.IsNotNull(result);
            var findResult = repo.FindOrderById(order.OrderId);
            //Test retrieve
            Assert.IsNotNull(findResult.Detail, "Detail");
            Assert.IsNotNull((findResult.Detail as SROrderPreToPostDtl).Customer, "Customer");
            Assert.IsNotNull((findResult.Detail as SROrderPreToPostDtl).CustAddress.ID, "Address");
            //Assert.IsNotNull(findResult.Detail.VerificationDocs, "Verification");
            Assert.IsNotNull(findResult.CenterOrg, "CenterOrg");
            //Test delete
            var deleteResult = repo.DeleteOrderById(order.OrderId);
            Assert.IsTrue(deleteResult > 0);
        }
    }
}
