﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Repositories;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Enums;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.Common;
using System.Linq;
using System.Collections.Generic;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class CorpSimRepTest
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void CreateNewOrderAndDeleteShouldSuccess()
        {
            var order=new SROrderMaster() {
                CreateDt=DateTime.Now,
                CreateBy="ccklcc1",
                CenterOrgId=269,
                RequestType=RequestTypes.CorpSimReplace,
                Status=SRStatus.New,
            };
            order.VerificationDocs = new List<VerificationDoc>() {
                new VerificationDoc() {File="abc",FileName="abc.jpg",DocType=DocTypes.IdBack ,CreateBy = "ccklcc1",CreateDt=DateTime.Now},
                new VerificationDoc() {File="front",FileName="front.jpg",DocType=DocTypes.IdFront ,CreateBy = "ccklcc1",CreateDt=DateTime.Now}
            };
            var orderDtls= new SROrderCorpSimRepDtl() {
                MSISDN="01996211691",
                AccountNo="1232323",
                CustBiometricPass=false,
                NewSimSerial="1234567890123456789",
                SignDt=DateTime.Now,
                Remarks="something",
                QueueNo="12121",
                Justification="some justification"
            };
            orderDtls.Customer = new Customer() {
                IDCardTypeID=1,
                IDCardNo="881111111111",
                FullName="Jazz Tong",
                PayModeID=1,
                CreateDT=DateTime.Now,
                LastAccessID="ccklcc1",
                LastUpdateDT=DateTime.Now
            };
            order.Detail = orderDtls;
            //Test save
            var result=repo.SaveOrder(order);
            Assert.IsNotNull(result);
            var findResult = repo.FindOrderById(order.OrderId);
            //Test retrieve
            Assert.AreEqual(order.ID,findResult.ID);
            //Test other sub data load
            Assert.IsTrue(order.VerificationDocs.Count>0, "Verification");
            Assert.IsNotNull(findResult.Detail, "Detail");
            //Test detail
            TestDetail(orderDtls, findResult.Detail as SROrderCorpSimRepDtl);
            Assert.IsNotNull((findResult.Detail as SROrderCorpSimRepDtl).Customer, "Customer");
            Assert.IsNotNull(findResult.CenterOrg, "CenterOrg");
            //Test delete
            var deleteResult=repo.DeleteOrderById(order.OrderId);
            var deleteAndFindResult = repo.FindOrderById(order.OrderId);
            Assert.IsNull(deleteAndFindResult, "Delete result fail");
        }

        private void TestDetail(SROrderCorpSimRepDtl localobj, SROrderCorpSimRepDtl dbObj)
        {
            Assert.IsTrue(localobj.MSISDN.Equals(dbObj.MSISDN),"MSISDN not match");
            Assert.IsTrue(localobj.AccountNo.Equals(dbObj.AccountNo), "AccountNo not match");
            Assert.IsTrue(localobj.CustBiometricPass.Equals(dbObj.CustBiometricPass), "CustBiometricPass not match");
            Assert.IsTrue(localobj.NewSimSerial.Equals(dbObj.NewSimSerial), "NewSimSerial not match");
            Assert.IsTrue((localobj.SignDt - dbObj.SignDt) < TimeSpan.FromSeconds(1),"SignDt not match");
            Assert.IsTrue(localobj.Remarks.Equals(dbObj.Remarks), "Remarks not match");
            Assert.IsTrue(localobj.QueueNo.Equals(dbObj.QueueNo), "QueueNo not match");
            Assert.IsTrue(localobj.Justification.Equals(dbObj.Justification), "Justification not match");
        }
    }
}
