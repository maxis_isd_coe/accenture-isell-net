﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.DAL.Interfaces;
using Online.Registration.DAL.Models;
using Online.Registration.DAL.Models.FilterCriterias;
using Online.Registration.DAL.Models.ServiceRequest;
using Online.Registration.DAL.Models.ServiceRequest.Dto;
using Online.Registration.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Online.Registration.DAL.Helpers;

namespace Online.Registration.DAL.Tests.ServiceRequest
{
    [TestClass]
    public class QueryMasterTest
    {
        private static ISRRepository repo;
        [ClassInitialize()]
        public static void ClassInit(TestContext context)
        {
            repo = new SRRepository();
        }

        [TestMethod]
        public void RetrieveQueryMaster_ShouldReturnData()
        {
            using(var db=new OnlineStoreDbContext()){
                var result = db.SRQueryMasters.ToList();
                Assert.IsTrue(result.Count > 0, "Query master fail");
            }
        }

        [TestMethod]
        public void SimpleQueryMaster_ShouldReturnData(){
            var sr=new SRQueryMaster();
            //var result = repo.QueryOrder(new SRQueryOrderDto() {
            //    CriteriaList=new List<IFilterCriteria>(){
            //        //new FilterCriteria(sr.Name(n=>n.CreateBy),"svklcc1"),
            //        //new TodayFilterCriteria(sr.Name(n=>n.CreateDt))
            //        new StartDateFilterCriteria(sr.Name(n=>n.CreateDt),new DateTime(2015,11,1)),
            //        new EndDateFilterCriteria(sr.Name(n=>n.CreateDt),new DateTime(2015,11,5))
            //    },
            //    SortyBy=sr.Name(n=>n.FullName),
            //    SortDesc=false
            //});
            //Assert.IsTrue(result.Count() > 0, "Simple query fail");
        }
    }
}
