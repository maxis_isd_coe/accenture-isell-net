﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Principal;
using System.Net;
using Online.Registration.IntegrationService.Tests.iContractUpload;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Online.Registration.IntegrationService.Tests
{
    [TestClass]
    public class iContractExternalTest
    {
        string testFilePath=@"D:\dev\MaxisISellDev\MobilityDrop5_Paperless_Drop\Online.Registration.IntegrationService.Tests\Test.jpg";
        string uploadPath = "http://10.200.47.66:1001/others_DocLibrary/";

        [TestMethod]
        public void TestUpload_Retrieve_Should_Success()
        {

            try
            {
                CopyResult[] resultupload;
                //TestUpload
                using (var client = new iContractUpload.CopySoapClient())
                {
                    setCretential(client);
                    var Copyresults = client.CopyIntoItems(uploadPath, new string[] { uploadPath + "test.jpg" }, GetInfo(), GetFile(), out resultupload);
                }
                //TestRetrieve
                using(var client=new iContractSearch.SearchDocsClient()){
                    setCretential(client);
                    var req = new iContractSearch.retrieveDocumentsListRequest() {
                        iSellOrderID = "712790"
                    };
                    var searchResults = client.retrieveDocumentsList(req);
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        [TestMethod]
        public void TestUpload()
        {
            using(var client=new iContractUploadSvc.IiContractClient()){
                var result = client.UploadFiletoIcontract(
                    "others",
                    "770261",
                    "520517075267",
                    "60199621691",
                    "1700669136",
                    "DP",
                    "New IC",
                    "Maxis Centre KLCC i-Centre(CNTRL.00010)",
                    GetFile(),
                    "ccklcc1",
                    "Front.jpg"
                    );
            }
        }


        private FieldInformation[] GetInfo()
        {
            var file = new Dictionary<string, string>() { 
                {"MSISDN","60199621691"},
                {"AgentCode","ccklcc1"},
                {"IDcardvalue","520517075267"},
                {"iSellOrderID","SR100261"},
                {"AccountNumber","1234567890"},
                {"StoreID","KLCC"},
                {"TransactionType","SR"},
                {"IDType","New IC"}
            };
            return file.Select(n=> new FieldInformation(){
                Id=Guid.NewGuid(),
                DisplayName=n.Key,
                InternalName=n.Key,
                Type=FieldType.Text,
                Value=n.Value
            }).ToArray();
        }

        private byte[] GetFile()
        {
            return File.ReadAllBytes(testFilePath);
        }

        private void setCretential(iContractSearch.SearchDocsClient _client)
        {
            _client.ClientCredentials.Windows.AllowNtlm = true;
            _client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.None;
            _client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("ICONSTG", "iC0nSta8!ng", "isddc");
        }

        private void setCretential(iContractUpload.CopySoapClient _client)
        {
            _client.ClientCredentials.Windows.AllowNtlm = true;
            _client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.None;
            _client.ClientCredentials.Windows.ClientCredential = new NetworkCredential("ICONSTG", "iC0nSta8!ng", "isddc");
        }
    }
}
