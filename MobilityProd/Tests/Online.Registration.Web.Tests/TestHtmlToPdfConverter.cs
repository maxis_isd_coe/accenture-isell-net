﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Online.Registration.Web.Areas.ServiceRequest.Controllers;
using Online.Registration.Web.CustomExtension;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Security.Principal;
using System.Collections.Specialized;

namespace Online.Registration.Web.Tests
{
    [TestClass]
    public class TestHtmlToPdfConverter
    {
        [TestMethod]
        public void SimpleHtmlToPdfConvert()
        {
            var htmlPath = @"D:\dev\MaxisISellDev\MobilityDrop5_Paperless_Drop\Online.Registration.Web.Tests\debug.htm";
            var htmlContent = File.ReadAllText(htmlPath);
            byte[] PDFBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(htmlContent);
            File.WriteAllBytes(@"c:\test.pdf", PDFBytes);
            Assert.IsTrue(File.Exists(@"c:\test.pdf"));
        }

        [TestMethod]
        public void SimpleHtmlToPdfFromContract()
        {
            var htmlPath = @"D:\dev\MaxisISellDev\MobilityDrop5_Paperless_Drop\Online.Registration.Web.Tests\debug_addcontract.htm";
            var htmlContent = File.ReadAllText(htmlPath);
            byte[] PDFBytes = (new NReco.PdfGenerator.HtmlToPdfConverter()).GeneratePdf(htmlContent);
            File.WriteAllBytes(@"c:\test2.pdf", PDFBytes);
            Assert.IsTrue(File.Exists(@"c:\test2.pdf"));
        }

    }
}
