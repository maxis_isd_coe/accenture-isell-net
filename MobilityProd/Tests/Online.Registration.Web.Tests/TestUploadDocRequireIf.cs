﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Online.Registration.Web.ViewModels.Common;
using Online.Registration.Web.CustomDataAnnotation;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Online.Registration.Web.Tests
{
    [TestClass]
    public class TestUploadDocRequireIf
    {
        public class TestUploadDocVM{

            public bool BiometricPass { get; set; }
            
            [UploadDocRequireIf("BiometricPass",false,"Something wrong")]
            public UploadDocVM UploadDoc { get; set; }
        }

        public class TestUploadDocOtherVM{
            public bool BiometricPass { get; set; }

            [UploadDocRequireIf("BiometricPass", false, "Something wrong")]
            public OtherUploadDocVM UploadDoc { get; set; }
        }

        public class OtherUploadDocVM {
        }

        [TestMethod]
        public void Should_Fail_When_UploadDocNull()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = false;
            var result = Validate(vm);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void Should_Fail_When_FrondIdFileNull()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = false;
            vm.UploadDoc = new UploadDocVM() {
                BackIdFile="something"
            };
            var result = Validate(vm);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void Should_Fail_When_BackIdFileNull()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = false;
            vm.UploadDoc = new UploadDocVM()
            {
                FrontIdFile="something"
            };
            var result = Validate(vm);
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void Should_Pass_When_BiometricPass_WithoutUploadDoc()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = true;
            var result = Validate(vm);
            Assert.IsTrue(result.Count==0);
        }

        [TestMethod]
        public void Should_Pass_When_BiometricFail_With_UploadDoc()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = false;
            vm.UploadDoc = new UploadDocVM()
            {
                FrontIdFile = "something",
                BackIdFile="something"
            };
            var result = Validate(vm);
            Assert.IsTrue(result.Count == 0);
        }
        [TestMethod]
        public void Should_Pass_When_BiometricPass_With_UploadDoc()
        {
            var vm = new TestUploadDocVM();
            vm.BiometricPass = true;
            vm.UploadDoc = new UploadDocVM()
            {
                FrontIdFile = "something",
                BackIdFile = "something"
            };
            var result = Validate(vm);
            Assert.IsTrue(result.Count == 0);
        }
        [TestMethod]
        public void Should_Fail_If_Not_Use_UploadVM()
        {
            try
            {
                var vm = new TestUploadDocOtherVM();
                vm.BiometricPass = true;
                vm.UploadDoc = new OtherUploadDocVM();
                var result = Validate(vm);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message != "");
            }
        }

        private List<ValidationResult> Validate(object vm)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(vm, null, null);
            Validator.TryValidateObject(vm, ctx, validationResults, true);
            return validationResults;
        }
    }
}
