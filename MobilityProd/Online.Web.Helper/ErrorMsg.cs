﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Online.Web.Helper
{
    public class ErrorMsg
    {
        public static class Account
        {
            public static string DuplicateUserName = "User name already exists. Please enter a different user name.";

            public static string DuplicateEmail =
                "A user name for that e-mail address already exists. Please enter a different e-mail address.";

            public static string InvalidPassword =
                "The password provided is invalid. Please enter a valid password value.";

            public static string InvalidEmail = "The e-mail address provided is invalid. Please check the value and try again.";

            public static string InvalidAnswer = "The password retrieval answer provided is invalid. Please check the value and try again.";

            public static string InvalidQuestion = "The password retrieval question provided is invalid. Please check the value and try again.";

            public static string InvalidUserName = "The user name provided is invalid. Please check the value and try again.";

            public static string ProviderError = "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            public static string UserRejected = "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            public static string DefaultMembershipCreateStatus =
                "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }

        public static class VAS
        {
            public static string FetchData = "OPF system error - Unable to fetch data.";
            public static string ComponentNotAvailable = "Selected components are not available.";
        }

        public static class Cache
        {
            public static string ClearCacheSuccess = "Cache has been Cleared successfully";
            public static string NoCache = "No Cache data to Cleared";
            public static string ClearCacheFail = "problem while clearing the cache data Please try again.";
        }

        public static class Registration
        {
            public static string CardLength = "Card number length does not meet";
            public static string CardRange = "Card number range does not meet";
            public static string CardError = "Invalid credit card";
            public static string DDMFFail = "DDMF Check Failed";
            public static string AgeFail = "Age is not valid";
            public static string AddressFail = "Address is not valid";
            public static string ContractFail = "Contract Check Failed";

            //GTM e-Billing CR - Ricky - 2014.11.20
            public static string BillDelivery_DeliveryEmpty = "Please choose one of the Bill Delivery options.";
            public static string BillDelivery_SMSNotifEmpty = "Please choose whether SMS notification is required or not.";
            public static string BillDelivery_EmailEmpty = "Email Address (Bill Delivery Options) cannot be empty.";
            public static string BillDelivery_EmailNotValid = "Email Address (Bill Delivery Options) is not valid.";
            public static string BillDelivery_SMSNumberEmpty = "Please choose the Mobile Number for SMS notification.";

            public static string CRP_SameRatePlan = "Unable to proceed with same rate plan. Please select another plan.";
        }
    }
}
